
* TOOLS

** add oscillation analysis - phase, amplitude, average cycle
** split data conversion and plotting; store converted data
** cleaner unit and data conversion tools


* DATA

** period data and oscillations

\cite{Zhang2021}: 132 h period at mu=0.027/h (25 h), metabolic and transcriptome data

\cite{Wang2000, Uno2002, Kwak2003, Slavov2012}: stress-resistance ~ HOC/LOC

\cite{Lloyd02b}: Cycles of mitochondrial energization; electronmicroscopy of ifo0233; ethanol, H2S, NAD(P)H, acetaldehyde

\cite{ONeill2020pre}: several nice data sets; glycogen, survival rate, G1/G2 ratio, effect of potassium, various metabolites, ions

\cite{Blank2009}: Fig. 1, Abf2p overexpression causes increased mtDNA
and slight period lengthening (5.3 -> 5.9 h), dilution rate and all
other conditions identical to tu05

\cite{Laxman2013} - Fig 2, different periods in different mutants

\cite{Martegani1990} - Fig 1, two waves of BI during one DO osci

\cite{Murray2004} and \cite{Mochan1973} - 0-growth

\cite{Gowans2018} - CEN.PK periods in INO80 mutants

** batch data

\cite{Ju1994}: ribosome concentrations over growth

\cite{Lilli1980} - glycogen data (and get more)

** oscillation data

\cite{Burnetti2016} - S-Phase
