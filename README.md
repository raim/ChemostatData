# YEAST CHEMOSTAT DATA COLLECTION

## How to Add Data

To collect data from publication figures and tables:

1) Generate a log file in dir log/ with file name
firstauthor_year_digitization.txt .

In this file, record all relevant information including the used
strain, medium and reactor settings; see some existing files as a 
guide.

2) Copy images from the publication, e.g., select image
area in Acrobat Reader and copy to clipboard, and paste
in gimp ("create new > from clipboard").

3) Digitize figures using e.g. engauge or 
http://arohatgi.info/WebPlotDigitizer/app/, or 
copy tables into text or spreadsheet files.

Please post-process digitized figures and tables as necessary but
try to stay close to the original values and units of the original
publication, AND record all processing steps and
calculations (e.g. as R code) in the log file!

4) Save images and digitized data files as csv in originalData/ 
using the same firstauthor_year prefix, then a data index,
e.g. firstauthor_year_fig5A.png, firstauthor_year_fig5a.csv

5) If the recorded data fits into `periods.csv` or
`growthrates.csv` add the data there. Note the conventions
on names and on fields with multiple entries!


## Directories and Files

* `log`: log of digitization of individual papers
* `originalData`: captured images and digitized data
* files in main directory:
    - `periods.csv*: data on dilution rate vs. oscillation periods,
    - `growthrates.csv`: data on dilution rate vs. various measurements (metabolism, cell division cycle).

## Detailed File Guide

* Columns in `periods.csv`:
    - oscillation period vs. dilution rate and/or generation times
    - D dilution rate in /h
    - t2 culture doubling time (only one of D and t_2 required, t_2 = ln 2 / D
    - P period of metabolic oscillation, lower value if range is given
    - P_max upper value, if range was given for period
    - num minimum number of cycles observed or shown, s for stable, or time over which stable oscillations occured
    - phases measured parameters that oscillate and their phases; NP: nucleotide pool; AP: adenosine-phosphate total; FSZ: freshly budded cells; DZ1: budded cells mid-phase; DZ2: budded cells end-phase; PBI: parent cell budding index; DBI: daughter cell budding index
    - medium composition per L
    - cellosc oscillations in optical density or cell numbers? logical
    - A aeration, usually in vvm, ie., volume per working volume per minute
* Columns in `growthrates.csv`:
    - TODO






























