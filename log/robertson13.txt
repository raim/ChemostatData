
publication: pmid, 24297928; Visible light alters yeast metabolic rhythms by inhibiting respiration. Robertson et al. PNAS 2013

strain: Saccharomyce cerevisiae strain CEN.PK113-7D
medium: glucose, 10 g; (NH4)2SO4, 5 g; MgSO4.7H2O, 0.5 g; yeast extract (Becton Dickinson), 1 g; KH2PO4, 2 g; H2SO4 (70%), 0.51 mL; antifoam A (Sigma), 0.5 mL; CaCl2, 0.125 mmol; FeSO4.7H2O, 20 mg; ZnSO4.7H2O, 10 mg; CuSO4.5H2O, 5 mg; MnCl2.4H2O, 1 mg
culture: 3 L New Brunswick Scientific Bioflo 115 Bioreactor; dilution rate, ca 0.085 h-1

NOTE: medium and culture as describedin Robertson2008

Fig. 1B

file - robertson13_fig1b.csv
x - light intensities 0, 90, 180, 300, 300* uE m-2 s-1 (for 12 h)
y - period, min

NOTE: “300*” represents the average period for the second and longer 300 
treatment

D=0.085 1/h
Periods, in h:
4.148783 
3.057933 
1.591560 
1.359085 
1.412732

Fig. 1D

file - robertson13_fig1d.csv
x - light colors, dark, red (80), blue (60), green (120), white (180) each for 12 h; number in () are uE m-2 s-1
y - period, min

NOTE: dimmer green light (80) had only minor effects

D=0.085 1/h
Periods, in h:
3.576017 
3.538983 
1.730750 
1.761050 
1.302183 
3.626633



NOTE: medium equivalent to Robertson2008
 
10 g/L anhydrous glucose, 
5 g/L ammonium sulfate, 
0.5 g/L magnesium sulfate heptahydrate, 
1 g/L yeast extract, 
2 g/L potassium phosphate,
0.5 mL/L of 70% v/v sulfuric acid, 
0.5 mL/L of antifoam A, 
0.5 mL/L 250 mM calcium chloride, and 
0.5 mL/L mineral solution A. 

(Mineral solution A consists of 
40 g/L FeSO4·7H2O, 
20 g/L ZnSO4·7H2O, 
10 g/L CuSO4·5H2O, 
2 g/L MnCl2·4H2O,and 
20 mL/L 75% sulfuric acid.)
