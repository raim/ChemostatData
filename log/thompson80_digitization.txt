
http://arohatgi.info/WebPlotDigitizer/app/?

Data from \cite{Thompson1980}, 
strain: S. cerevisiae S288C/1
medium: glucose, 20g; yeast extract, 10g ; bacteriological peptone, 20g
culture: Vol, 2 L; T, 30°C; pH, 5.5; Air, 1 vvm; Agit, 600 rpm
Air flow: 2L/min = 1 vvm
Agit: 600 rpm
Antifoam: 5 % (v/v) silicone DC antifoam emulsion M10 (Hopkin & Williams, Chadwell Heath, Essex)





Fig. 1 - 

x - doubling time [min]
y - duration of D, P or B [min], calculated from bud scar analyses after Lord&Wheals 1980

1:20 - D, daughter cycle, open circle
21:40 - P, parent cycle, filled circle
41:60 - B, budded period, open squares

TODO - align data points, D/P/B triplets


in R:
dat <- read.csv("thompson80_fig1_20150507.dat",header=F)
plot(dat,log="xy",col=c(rep(1,20),rep(2,20),rep(3,20)),xlab="doubling time [min]",ylab="D/P/B times [min]")
lines(50:200, 1.04*(50:200)+9,col=1) # D, fast
lines(50:200, 0.96*(50:200)-8,col=2) # P, fast
lines(50:200, 0.71*(50:200)+6,col=3) # B, fast
lines(200:800, 1.71*(200:800)-128,col=1) # D, slow
lines(200:800, 0.5 *(200:800)+ 82,col=2) # P, slow
lines(200:800, 0.11*(200:800)+108,col=3) # B, slow
## fig 2 - ratio D:P vs mu
plot(log(2)/dat[1:20,1], dat[1:20,2]/dat[21:40,2])
## TODO - align data points!

Table 1 - linear regression of different areas from above plot:
	D	P	B	P-B
fast	1.04*t+9	0.96*t-8	0.71*t+6	0.25*t-14
slow	1.71*t-128	0.5*t+82	0.11*t+108	0.39*t-26

Fig. 5 - cell volume, weight and density

Fig. 5a - cell volume

x - doubling time [min]
y - median cell volume [um^3]

1:18 data points

Fig. 5b - dry wt per cell

x - doubling time [min]
y - dry wt per cell [pg]

dwc <- read.csv("thompson80_fig5b_20150507.dat",header=FALSE)

d2 <- 60*log(2)/dwc[,1]
plot(d2,dwc[,2],type="b")

1:17 data points

Fig. 5c - density 

x - doubling time [min]
y - cell density [g/ml]; NOTE: assuming wrong conversion, should be pg/um^3

1:15 data points
