
Conditions: see periods.gnumeric for details
strain: Saccharomyces cerevisiae H 1022
medium: NL 18, 3% Glucose (7% H2O)
reactor: 10-Liter-Kleinfermenter (Chemap AG, Männedorf, Schweiz) mit automatischer Temperatur- und pH-Regulierung verwendet (FIECHTER, 1965); arbeitendes
Volumen (meist!) 
culture: V_tot, 10 L; V_L, >= 2.6 L (depends on dilution rate!, Fig. 1); air, 1-3 vvm; agit, 700 rpm; O2-transfer, 120 -150 mmol/l/h



NOTE: working volume usually 2.6 L, reactor volume 10 L gives a big
headspace; delay for offgas, but apparently not too big:
7.4 L  / 2*2.6 L/min = 1.4 min - TODO correct?

NOTE: 
3% glucose (7% H2O) converted to 27.9 g in periods.gnumeric 
3% and 1% glucose converted to 2.79 % and 0.93 % in growthrates.gnumeric

NOTE: NL 18 in periods.gnumeric!

NL 18 1%: Glucose (7% H20), 10 g; (NH4)2SO4, 2,5 g; KH2PO4,0,5 g; MgSO4.7H2O, 0,2 g; CaCl2.6H2O, 0,02 g; FeSO4(NH4)2SO4.6H2O, 4 mg; ZnSO4.7H2O, 2 mg; CuSO4.5H2O, 0,3 mg; m-Inosit, 20 mg; Thiamin, 4,4 mg; Pyridoxin, 1 mg; Ca-Pantothenat, 0,5 mg; d-Biotin, 0,03 mg; Glutaminsäure, 0,75 g; Hefeextrakt «Difco», 0,25 g

NL 16 entspricht NL 18, wobei dieses Medium an Stelle von Glutamat
Asparagin und keinen Hefeextrakt enthält.

NL 18 1%: glucose, 9.3 g; (NH4)2SO4, 2.5 g; KH2PO4, 0.5 g; MgSO4.7H2O, 0.2 g; CaCl2.6H2O, 0.02 g; FeSO4(NH4)2SO4.6H2O, 4 mg; ZnSO4.7H2O, 2 mg; CuSO4.5H2O, 0.3 mg; myo-inositol, 20 mg; thiamine, 4.4 mg; pyridoxine, 1 mg; Ca-pantothenate, 0.5 mg; biotin, 0.03 mg; glutamic acid, 0.75 g; yeast extract (Difco), 0.25 g

NL 18 3%: glucose, 27.9 g; (NH4)2SO4, 7.5 g; KH2PO4, 1.5 g; MgSO4.7H2O, 0.5 g; CaCl2.6H2O, 0.06 g; FeSO4(NH4)2SO4.6H2O, 12 mg; ZnSO4.7H2O, 6 mg; CuSO4.5H2O, 1 mg; myo-inositol, 60 mg; thiamine, 14 mg; pyridoxine, 3 mg; Ca-pantothenate, 1.5 mg; biotin, 0.1 mg; glutamic acid, 2.25 g; yeast extract (Difco), 0.75 g

NOTE: NL 18 30% taken from paper meyenburg69, doi: 10.1007/BF00414585

NL 16 1%:  glucose, 9.3 g; (NH4)2SO4, 2.5 g; KH2PO4, 0.5 g; MgSO4.7H2O, 0.2 g; CaCl2.6H2O, 0.02 g; FeSO4(NH4)2SO4.6H2O, 4 mg; ZnSO4.7H2O, 2 mg; CuSO4.5H2O, 0.3 mg; myo-inositol, 20 mg; thiamine, 4.4 mg; pyridoxine, 1 mg; Ca-pantothenate, 0.5 mg; biotin, 0.03 mg; asparagine, 0.75 g

NL 10 1%: glucose, 9.2 g; (NH4)2HPO4, 3.75 g; KH2PO4, 0.5 g; MgSO4.7H2O, 0.15 g; CaCl2.4H2O, 0.02 g;  FeSO4(NH4)2SO4.6H2O, 7.5 mg; ZnSO4.7H2O, 2 mg; CuSO4.5H2O, 0.3 mg; myo-inositol, 20 mg; thiamine, 4.4 mg; pyridoxine, 1.2 mg; Ca-pantothenate, 0.5 mg; biotin, 0.03 mg

NL 10 5%: glucose, 46 g; (NH4)2HPO4, 18.75 g; KH2PO4, 2.5 g; MgSO4.7H2O, 0.75 g; CaCl2.4H2O, 0.1 g;  FeSO4(NH4)2SO4.6H2O, 37.5 mg; ZnSO4.7H2O, 10 mg; CuSO4.5H2O, 1.5 mg; myo-inositol, 100 mg; thiamine, 22 mg; pyridoxine, 6 mg; Ca-pantothenate, 2.5 mg; biotin, 0.15 mg


NOTE: NL 10 1% taken from paper Meyenburg1968: Der Sprossungszyklus von Saccharomyces cerevisiae; assuming that scaling of nutrients was also done for NL10 in Tabelle 7 experiment
 

Fig. 9A - X vs. dilution rate

file - meyenburg69phd_fig9A_X.dat
x - dilution rate [/h]
y - X [g DW/L]
1:47 - X [g DW/L]

NOTE: medium NL 18 3%

Fig. 9A - RQ vs. dilution rate

file - meyenburg69phd_fig9A_RQ.dat
x - dilution rate [/h]
y - RQ 
1:39 - RQ 

NOTE: used same y-axis as above for X
NOTE: medium NL 18 3%

Fig. 9A - qCO2 and qO2 vs. dilution rate

file - meyenburg69phd_fig9A_qGAS.dat
x - dilution rate [/h]
y - qO2 and qCO2 [mMol/(g DW . h)]

1:41 - qO2  [mMol/(g DW . h)], filled circles
42:72 - qCO2 [mMol/(g DW . h)], closed circles

NOTE: medium NL 18 3%

in R:
dat <- read.csv("meyenburg69phd_fig9A_qGAS.dat",header=F)
o2 <- 1:41
co2 <- 42:72
plot(dat[o2,1],dat[o2,2],ylim=range(dat[,2]))
points(dat[co2,1],dat[co2,2],col=2) 

NOTE: add missing co2 terms in R
gas <- read.csv("originalData/meyenburg69phd_fig9A_qGAS.dat",header=F)
o2 <- 1:41
co2 <- 42:72
rq <- read.csv("originalData/meyenburg69phd_fig9A_RQ.dat",header=F)

## interpolate and calculate CO2
rqi <- approx(rq[,1],rq[,2],gas[o2,1])$y
co2i <- rqi*gas[o2,2]
ngas <- cbind(gas[o2,1],gas[o2,2],co2i)
colnames(ngas) <- c("D","qO2","qCO2")
write.csv(ngas,"originalData/meyenburg69phd_fig9A_qGAS_processed.dat",row.names=FALSE)

Fig. 9B - S (glucose) 

file - meyenburg69phd_fig9B_S.dat
x - dilution rate [/h]
y - S [g/L], filled triangles

13 data points

NOTE: medium NL 18 3%


Fig. 9B - PYR (pyruvate)

file - meyenburg69phd_fig9B_PYR.dat
x - dilution rate [/h]
y - PYR [g/L], open circles

14 data points

NOTE: medium NL 18 3%

Fig. 9A - A (ethanol)

file - meyenburg69phd_fig9B_A.dat
x - dilution rate [/h]
y - Ethanol [g/L], open triangles

NOTE: medium NL 18 3%

Table 6: D, Yield, qCO2, qO2, RQ; values for and from Fig. 9

file - meyenburg69phd_Tab6.csv
tool - manual copy

NOTE: calculated from data in Fig. 9A; medium NL 18 3%


Table 7: cell sizes

file - meyenburg69phd_tab7.csv
D - dilution rate, 1/h
V_cell - um^3
d_cell - um
DW/cell - pg
density - pg/um3

NOTE: much bigger cells then in Thompson et al. 1980!! shaky?
NOTE: medium NL 10 5%

Fig. 22a - ATP/AMP/ADP 

date - 20150515
tool - engauge
file - meyenburg69phd_fig22a.dig/.csv
x - dilution rate, 1/h
y - AP, ATP, ADP, AMP, umol/g DW

NOTE: medium NL 18 3%

Fig. 13 - RNA/DNA

date - 20150515
tool - engauge
file - meyenburg69phd_fig13_RNA.dig/.csv
x - dilution rate, 1/h
y - RNA, % DW
file - meyenburg69phd_fig13_DNA.dig/.csv
x - dilution rate, 1/h
y - DNA, % DW

NOTE: medium NL 16 1%



Fig.  34 - oscillation, ethanol, qCO2, qO2, X, qN, FSZ, RQ

NOTE: FSZ - frisch sprossende Zellen ~ BI

NOTE: assuming "mMol" are mmol in digitization

t2=9.5h -> D=0.073/h
tOSC = unclear, seems 1:1, 8.5h or more!!

date - 20170729
tool - engauge
x - time, min
file - meyenburg69phd_fig34_qGas.dig/csv
y - qCO2; qO2
units - mmol/g/h;mmol/g/h
file - meyenburg69phd_fig34_EtOH.dig/csv
y - EtOH
units - mg/L


Fig. 43 - oscillation, ATP, ADP, AMP, qO2, qCO2, Glucose, Biomass, Pyruvate

NL 18, 3 % Glucose
t2=4.1h -> D=0.17/h
tOSC=2.75h

date - 20170729
tool - engauge
x - time, h
file - meyenburg69phd_fig43a.dig/csv
y - qCO2
units - mmol/g/h
file - meyenburg69phd_fig43a_glc.dig/csv
y - glucose
units - mmol/g
file - meyenburg69phd_fig43b.dig/csv
y - ATP;ADP;AMP
units - umol/g;umol/g;umol/g

Fig. 45 - oscillation, ATP, ADP, AMP, Glucose, Biomass, Pyruvate

NL 18, 3 % Glucose
t2=3.1h -> D=0.22/h
tOSC=1.1h 

NOTE: higher then mode 1:2!


date - 20170729
tool - engauge
x - time, h
file - meyenburg69phd_fig45a_qCO2.dig/csv
y - qCO2
units - mmol/g/h
file - meyenburg69phd_fig45b_adenosin.dig/csv 
y - ATP;ADP;AMP
units - umol/g;umol/g;umol/g


