
strain: Saccharomyces cerevisiae IFO 0233; diploid
culture: Vol, 1.2 L; T, 30°C; Agit, 800 rpm; Air, 0.15 vvm (180 mL/min); pH 4 (2.5 N NaOH); D, 0.085, 1/h;
medium: ethanol (99.5%), 15.7 g; polypepton (Nihon Seiyaku), 1 g; yeast extract (Difco), 1 g; (NH4)2SO4, 5 g; KH2PO4, 2 g; MgSO4.7H20, 0.5 g; CaCl2.2H20, 100 mg; FeSO4.7H20, 20 mg; ZnSO4.7H20, 10 mg; CuSO4.5H20, 5 mg; MnCl2.4H2O , 1 mg; H2SO4 (70%), 1 mL; antifoam adecanol LG-294 (Asahidenka), 0.6 mL

NOTE: ethanol corrected to "ethanol, 15.6215 g" in periods.gnumeric

Fig 1: TODO
