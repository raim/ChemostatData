

strain: Saccharomyces cerevisiae IFO 0233; diploid
culture: Vol, 1.35 L; T, 30°C; Agit, 700 rpm; Air, 0.15 vvm (200 mL/min); pH 4 (20% NaOH);
medium: glucose, 20 g; yeast extract (Difco), 1 g; (NH4)2SO4, 5 g; KH2PO4, 2 g; MgSO4.7H20, 0.5 g; CaCl2.2H20, 100 mg; FeSO4.7H20 , 20 mg; ZnSo4.7H20 , 10 mg; CuSO4.5H20 , 5 mg; MnSO4.5H20 , 1 mg; antifoam agent (Adecanol LG-294; Asahidenka, Japan), 0.05 ml. [all changed accordingly if higher Glc was used]

NOTE: For calculation of intracellular concentrations of metabolites, the cytosolic volume was assumed to be 2 ml/g dry cells [16].


Fig 1, osci: qO2:0°; qCO2:60°; Glycogen_i:0°; Trehalose_i:0°; Glc:60°; EtOH:-120°;NADH:-60°;qTCA:0°; qGlyc:60°
Fig 2, osci: qO2:0°;Pyr:-10°;Pyr_i:0°;Ace:180°;Ace_i:0°;pH_i:0°;Glycogen_i:0°; EtOH:-120°;ATP:60°; ;qTCA:0°; qGlyc:60°

Fig 1a 

tool - engauge, R to correct y for EtOH
file - satroutdinov92_fig1a_corrected.csv
x - time, min
y - Glc;EtOH
units - g/L;g/L

in R:
dat <- read.csv("originalData/satroutdinov92_fig1a.csv",check.names=FALSE); dat[,3] <- 1.3 + 0.5*dat[,3]/20; write.csv(dat,file="originalData/satroutdinov92_fig1a_corrected.csv",row.names=F)

Fig 1b

tool - engauge, R to correct y for Glycogen
file - satroutdinov92_fig1b_corrected.csv
x - time, min
y - trehalose;glycogen
units - mg/g;mg/g

in R: 
dat <- read.csv("originalData/satroutdinov92_fig1b.csv",check.names=FALSE); dat[,3] <- 25 + 20*dat[,3]/20; write.csv(dat,file="originalData/satroutdinov92_fig1b_corrected.csv",row.names=F)

Fig 1c

tool - engauge
file - satroutdinov92_fig1c.csv
x - time, min
y - qO2;qCO2
units - mmol/(g*h);mmol/(g*h)

Fig 1d 

tool - engauge
file - satroutdinov92_fig1d.csv
x - time, min
y - qTCA;qGlyc
units - mmol/(g*h);mmol/(g*h)

Fig 1e

tool - engauge
file - satroutdinov92_fig1e.csv 
x - time,min
y - Pi;NADH
units - mMol;mMol

Fig 2a

date - added units, 20170729
tool - engauge, R to correct y for glycogen
file - satroutdinov92_fig2a_corrected.csv
x - time,min
y - EtOH;Glycogen
units - g/L;mg/g

in R:
dat <- read.csv("originalData/satroutdinov92_fig2a.csv",check.names=FALSE); dat[,3] <- 20 + 20*(dat[,3]-1)/0.5; write.csv(dat, "originalData/satroutdinov92_fig2a_corrected.csv",row.names=F)

Fig 2b

tool - engauge
file - satroutdinov92_fig2b.csv
x - time,min
y - qO2;ATP
units - mmol/(g*h);mMol

Fig 2c

tool - engauge
file - satroutdinov92_fig2c.csv
x - time,min
y - qGlyc;qTCA
units - mmol/(g*h);mmol/(g*h)

Fig 2d

tool - engauge, R to correct y for Pyr_in
file - satroutdinov92_fig2d_corrected.csv
x - time, min
y - Pyr;Pyr_in
units - mMol

in R:
dat <- read.csv("originalData/satroutdinov92_fig2d.csv"); dat[,3] <- 10*dat[,3]/0.02; write.csv(dat,file="originalData/satroutdinov92_fig2d_corrected.csv",row.names=F)


Fig 2e

tool - engauge, R to correct y for Ace_in
file - satroutdinov92_fig2e_corrected.csv
x - time, min
y - Ace;Ace_in
units - mMol

in R:
dat <- read.csv("originalData/satroutdinov92_fig2e.csv"); dat[,3] <- 120*dat[,3]/2.5; write.csv(dat,file="originalData/satroutdinov92_fig2e_corrected.csv",row.names=F)


### Calculate overall yields

NOTE: mean values calculated from digitized figures in R

Fig 1
D: 0.08 /h
glucose, medium: 22 g/L 
DW: 7.68 g/l
qO2: 1.5 - 5 mmol/(g*h) (mean 3.125032 mmol/(g*h))
qCO2: 3.5 mmol/(g*h) (mean 3.519678 mmol/(g*h))
EtOH: 1.45-1.65 g/L (mean 1.564852 g/L)
Glc: 0.00115 g/L (mean 0.001148796 g/L)

NOTE: manually put these values in
file - originalData/satroutdinov92_fig1_average.csv
which is used in growthrates.gnumeric

D,DW,EtOH,Glc,qO2,qCO2
0.08,7.68,1.564852,0.001148796,3.125032,3.51967


Fig 2
D: 0.079 /h
glucose, medium: 20 g/L
DW:  7.53 g/l
RQ: 0.8-2
qO2: 1.8 - 4.5 mmol/(g*h)
qCO2: NA
EtOH: 1.1-1.4 g/L
Glc: NA

Keulers1996a:
Tab 1
glucose, medium: glucose monohydrate, 22 g/L
DW: 8.1 g/L
EtOH: 36 mMol = 36 * 46.07  mmol/L * g/mol = 1.659 g/L


METHOD - GLYCOGEN

Glycogen was extracted by 0.5 N hot sodium
carbonate solution for 90 min and degraded by
amyloglucosidase (No. 102857, Boehringer, F R G )
to glucose [15].
...
glycogen as glucose were assayed spectrophotometrically. 
[15] Becker, J.U. (1978) Anal. Biochem. 86, 56-64.



