
strain: Saccharomyces cerevisiae 248 UNSW 703100
medium: as described Barford & Hall, 1976, 1979b; Pamment & Hall, 1978,
glucose 9.93 g/L

medium, Barford & Hall 1976:  in [g/c]
glucose, 20 g; (NH4)2SO4, 15.6 g; KH2PO4, 6.8 g; Na2HPO4, 0.22 g; citric acid, 0.42 g; MgCl2, 50 mg; FeCl3.6H2O, 28 mg; CaCl2, 7 mg; MnCl2.4H2O, 5 mg; CoCl2.6H2O, 1.2 mg; ZnCl2, 4 mg; CuCl2.2H2O, 0.8 mg; H3BO3, 0.4 mg; biotin, 0.16 mg; p-amino benzoic acid, 16 mg; riboflavin-5'P, 16 mg; pyridoxine-HCl, 32 mg; thiamine, 32 mg; Ca-pantothenate, 32 mg; nicotinic acid, 32 mg; myo-inositol, 160 mg

NOTE: "culture methods were as described previously (Barford & Hall, 1976, 1979b; Pamment & Hall, 1978)"; using medium above (1976) but with 9.93 g glucose; assuming: 



NOTE: corrections to qCO2 or qO2???

NOTE: all data collected in file barford90_all.csv

where also gas was converted from volume to mmol via the gas law
T = 298.15 #K
P =  101.325 #J/L
Na = 6.02214179e23 #n/mol
R = 8.314472 #J/(mol*K)

n = P*V/(R*T) 


Fig. 1 - X vs. dilution

x - dilution rate [/h]
y - dry weight [g/L]

11 data points

Fig. 1 - qCO2 and qO2 vs. dilution

x - dilution rate [/h]
y - CO2 and O2

1:10 - qCO2 [ml/(g*h)]
11:21 - qO2 [ml/(g*h)]


in R:
dw <- read.csv("barford79_fig1_X.dat",header=F)
dat <- read.csv("barford79_fig1_GAS.dat",header=F)

## TODO : convert from ml gas to mol

po <- 1
co2 <- 1:10
o2 <- 11:21
plot(dat[co2,1],dat[co2,2],xlim=c(0,.6),ylim=c(0,800),type="l")
lines(dat[o2,1],dat[o2,2],col=2)
lines(dat[o2,1],2*po*dat[o2,2],col=3)

# interpolate CO2 to the O2 dilution rates
d <- round(dat[o2,1],2)
co2dat <- approx(round(dat[co2,1],2),dat[co2,2],d)$y
dwdat <- approx(round(dw[,1],2),dw[,2],d)$y
o2dat <- dat[o2,2]

atp_fr <- co2dat - o2dat
atp_ox <- 2*po*o2dat

plot(d, co2dat,xlim=c(0,.6),ylim=c(0,1350),type="l")
lines(d,o2dat,col=2)
lines(d,(atp_fr+atp_ox),col="blue")
par(new=T)
plot(d,atp_fr/(atp_ox+atp_fr),xlab=NA,ylab=NA,axes=FALSE,ylim=c(0,1),type="l",col=3,xlim=c(0,.6))
lines(d,d*dwdat,col=5)
axis(4)

pdf("barford79_fig1_digitized.pdf",width=5,height=5)
par(mfcol=c(2,1),mai=c(.1,.7,.1,.7))
plot(d, co2dat,xlim=c(0,.6),ylim=c(0,1200),type="b")
lines(d,o2dat,col=2,type="b")
lines(d,(atp_fr+atp_ox),col=4,type="b")
legend("topleft",col=c(1,2,4), lty=1,legend=c("qCO2","qO2","qATP"))
plot(d,dwdat,type="b")
par(new=T)
plot(d,d*dwdat,type="b",col=5,axes=F,ylab="",xlab="")
axis(4)
legend("right",col=c(1,5),lty=1,legend=c("X","X*D"))
dev.off()

