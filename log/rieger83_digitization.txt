20150515 - http://arohatgi.info/WebPlotDigitizer/app/?

strain: Saccharomyces cerevisiae H 1022
medium:  (mol carbon-1): (NH4)2S04, 6 g; (NH4)2HP04, 1.95 g; KCl, 0.9 g; 
MgS04.7H20, 0.45 g; CaCl2.2H20,0.3 g; CuS04.5H20, 2.34 mg; FeCl3.6H20, 14.4 mg; 
ZnS04.7H20,9 mg; MnSO4.2H20, 10.5 mg; biotin, 0.03 mg; myo-inositol, 60 mg; 
calcium pantothenate, 30 mg; thiamin hydrochloride, 6 mg; 
pyridoxine hydrochloride, 1.5 mg; 
antifoam: 0.25 ml of polypropylene glycol 2000 (Fluka, Switzerland) 
culture: Vol, 2.5 L; pH, 5; T, 30°C

NOTE: medium calculation, using 30 g Glc ~ 1 C-mol 

medium  30%: glucose, 30 g; (NH4)2S04, 6 g; (NH4)2HPO4, 1.95 g; KCl, 0.9 g; MgSO4.7H2O, 0.45 g; CaCl2.2H2O, 0.3 g; CuS04.5H2O, 2.34 mg; FeCl3.6H2O, 14.4 mg; ZnSO4.7H2O, 9 mg; MnSO4.2H2O, 10.5 mg; biotin, 0.03 mg; myo-inositol, 60 mg; Ca-pantothenate, 30 mg; thiamine-HCl, 6 mg; pyridoxine-HCl, 1.5 mg;
medium   1%: glucose, 10 g; (NH4)2S04, 2 g; (NH4)2HPO4, 0.65 g; KCl, 0.3 g; MgSO4.7H2O, 0.15 g; CaCl2.2H2O, 0.1 g; CuS04.5H2O, 0.78 mg; FeCl3.6H2O, 4.8 mg; ZnSO4.7H2O, 3 mg; MnSO4.2H2O, 3.5 mg; biotin, 0.01 mg; myo-inositol, 20 mg; Ca-pantothenate, 10 mg; thiamine-HCl, 2 mg; pyridoxine-HCl, 0.5 mg;
medium 0.5%: glucose, 5 g; (NH4)2S04, 1 g; (NH4)2HPO4, 0.325 g; KCl, 0.15 g; MgSO4.7H2O, 0.075 g; CaCl2.2H2O, 0.05 g; CuS04.5H2O, 0.39 mg; FeCl3.6H2O, 2.4 mg; ZnSO4.7H2O, 1.5 mg; MnSO4.2H2O, 1.75 mg; biotin, 0.005 mg; myo-inositol, 10 mg; Ca-pantothenate, 5 mg; thiamine-HCl, 1 mg; pyridoxine-HCl, 0.25 mg;

Table 1 - growth on different substrates

file - rieger83_Tab1.gnumeric, rieger83_Tab1.csv
x - dilution rate, 1/h
y - DW;Glc;Y;EtOH;qO2;qCO2;RQ


NOTE: skipping DW and qO2 values since these are available from
the Figures below

Fig. 1 - X

file - rieger83_fig1_X.csv
x - dilution [1/h]
y - biomass [g/L]

1:21 - 30 g glucose/L, circles
22:36 - 10 g glucose/L, squares
37:51 - 5 g glucose/L, triangles

Fig. 1 - qO2

file - rieger83_fig1_qO2.csv
x - dilution [1/h]
y - qO2 [mumol/(g*L)] - NOTE: probably mmol!

1:8 - 30 g glucose/L, circles
9:15 - 10 g glucose/L, squares
16:22 - 5 g glucose/L, triangles


Fig. 1 - qGlc

file - rieger83_fig1_substrate.csv
x - dilution [1/h]
y - substrate uptake rate [mumol/(g*h)] - NOTE: units? see above

1:6 - 30 g glucose/L, circles
7:12 - 10 g glucose/L, squares
13:17 -  5 g glucose/L, triangles


