
strain: S. cerevisiae strain CEN.PK113-7D luciferase reporter
medium: 10 g/L anhydrous glucose, 5 g/L ammonium sulfate, 0.5 g/L
magnesium sulfate heptahydrate, 1 g/L yeast extract, 2 g/L potassium phosphate,
0.5 mL/L of 70% v/v sulfuric acid, 0.5 mL/L of antifoam A, 0.5 mL/L 250 mM
calcium chloride, and 0.5 mL/L mineral solution A. (Mineral solution A consists of 40 g/L FeSO4 · 7H2O, 20 g/L ZnSO4 · 7H2O, 10 g/L CuSO4 · 5H2O, 2 g/L MnCl2 ·
4H2O,and 20 mL/L 75% sulfuric acid.)
culture: 3 L New Brunswick Scientific Bioflo 110 or 115 Bioreactor
