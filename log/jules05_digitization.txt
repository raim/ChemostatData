
strain: Saccharomyces cerevisiae CEN.PK113-7D (MATa, MAL2–8c, SUC2)
 and  mutant atg1, (MATa, MAL2–8c, SUC2, agt1D::kanMX4)
medium: trehalose, 2 g (% w/v); YN, 1L; orthophosphoric acid, 4.8 pH


culture: Vol, 1.5 L; pH, 4.8 (2 M NaOH). T, 30 °C; Agit, var; DO, >20 %; 
Air, 0.111 vvm (10 L/h)


Fig 1c and Tab1  TODO

