
strain: Saccharomyces cerevisiae DS 28911;  obtained from Gist-Brocades BV, Delft, The Netherlands.
culture: Vol, 1 L; T, 30 °C; Agit, 800 rpm; pH, 5 (2 M KOH); Air, 0.5 vvm (0.5 L/min); DO, >60 %
medium: glucose, 7.5 g; (NH4)2SO4, 5 g; KH2PO4, 3 g; MgSO4.7H2O, 0.5 g; 
EDTA, 15 mg; ZnSO4.7H2O, 4.5 mg; CoCl2.6H2O, 0.3 mg; MnCl2.4H2O, 1 mg; 
CuSO4.5H2O, 0.3 mg; CaCl2.2H2O, 4.5 mg; FeSO4.7H2O, 3 mg; NaMoO4.2H2O, 0.4 mg;
H3BO3, 1 mg; KI, 0.1 mg; silicone antifoam (BDH), 0.025 ml; 
biotin, 0.05 mg; Ca-pantothenate, 1 mg; nicotinic acid, 1 mg; inositol, 25 mg; thiamine-HCl, 1 mg; pyridoxine-HCl, 1 mg; para-aminobenzoic acid, 0.2 mg



Table 1

date - 20150522
tool - copied from pdf to gnumeric
file - vanhoek98_tab1.gnumeric/,.csv
x - dilution rate, 1/h
y - Y;qO2;qCO2;qGlc;qEtOH;qAce;qPyr;qGlycerol;Carbon_recovery
units - g/g;mmol/(g*h);mmol/(g*h);mmol/(g*h);mmol/(g*h);mmol/(g*h);mmol/(g*h);mmol/(g*h);%



Fig 1b - qO2, qCO2

date - 20150522
tool - engauge
file - vanhoek98_1b.csv
x - dilution rate, 1/h
y - qO2, qCO2, mmol/(g*h)

Fig 1a - qEtOH, Y

date - 20150522
tool - engauge
file - vanhoek98_1b.csv
x - dilution rate, 1/h
y1 - EtOH, mmol/(g*h)
y2 - Y, g/g, 1:0.002

NOTE: re-scale y2

Fig 1c - protein

date - 20150522
tool - engauge
file - vanhoek98_1c.csv
x - dilution rate, 1/h
y - protein, % of DW

NOTE: fused all figure 1 and aligned x axis into
file - vanhoek_fig1.gnumeric/.csv
x - dilution rate, 1/h
y - qEtOH;Y;qO2;qCO2;protein
units - mmol/(g*h);g/g;mmol/(g*h);mmol/(g*h);%
