
strain: Saccharomyces cerevisiae CBS 426
culture: Vol, 1.35 L; D, 0.01 1/h; T, 30 °C; pH, 5 (4 M NaOH); Agit, 600 rpm; Air, 0.63 vvm (0.85 L/min)
medium: glucose, 20 g; (NH4)2SO4, 9 g; KH2PO4, 3 g; MgSO4.7H2O, 0.6 g; CaCl2.2H2O, 0.3 g; NaCl, 0.7 g; antifoam (Poly-propylene glycol P2000), 0.1 ml; yeast extract (Oxoid), 1 g; H3BO3, 15 mg; ZnSO4.7H2O, 30 mg; MnCl2, 32 mg; FeCl3.6H2O, 100 mg; CuSO4.5H2O, 0.8 mg; Na2MoO4.2H2O, 5 mg; KI, 2 mg; biotin, 0.1 mg; m-inositol, 160 mg; beta-alanine, 40 mg; folic acid, 2 mg; para-amino benzoic acid, 10 mg; nicotinic acid, 15 mg; pyridoxine hydrochloride, 10 mg; thiamin hydrochloride, 4 mg; riboflavin, 10 mg

oscillation: period 4.2 h at dilution: 0.1 /h, typical cell cycle osci, 
incl. waveform and Biomass osci

NOTE: added data to periods.gnumeric

TODO: digitize figures


Fig 1 - CER/OUR/DO/heat production rate/EtOH/Ace/Biomass

