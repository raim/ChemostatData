
strain: Saccharomyces cerevisiae DBY12007; diploid; S288c background and wild type HAP1 alleles, MATa/MATalpha, HAP1+
culture: reactor, Lambda; Vol, 2 L; T, 30 °C; initial cells, 1000 cells/mL
medium: glucose, 2 g; YNB, 1 L

Fig S2B - cell density vs. time

NOTE: corresponds to main experiment in Fig 2

file - slavov14_figS2B.csv
tool - engauge
x - time, h
y - cell density, cells/mL

NOTE: original cell densities for Fig. S2B sent by Slavov
on 11 Oct 2015 - slavov14_Figure_S2B_CellDensity.txt

Fig 2C - cell volume dist. vs. time

file - slavov14_figS2C.csv
tool - engauge
x - time, h
y1 - large cells (black upper area); cell volume, um^3
y2 - small cells (black lower area); cell volume, um^3
y3 - main, intermediate size (brightest area); cell volume, um^3

NOTE: heatmap, black area represents ca. 1.3% of cells, yellow area 2-3 % of cells

Fig S3 - glucose/etoh vs. time

tool - engauge
file - slavov14_fig3.csv
x - time, h
y1 - glucose, mM
y2 - ethanol, mM

Fig 2 - rCO2, rO2, RQ, qO2, qCO2 vs. time

tool - engauge
file - slavov14_fig2_rq.csv
x - time, h
y - RQ, dimensionless

Fig 3 - qATP, viability vs. time

tool - engauge
file - slavov14_fig3_atp.csv
x - time, h
y - ATP, pmol/(cell*h)


