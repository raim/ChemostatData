
TODO: culture-info, low cell dens - Monod kinetics!!??
TODO: add to growthrates.gnumeric
TODO: check data!

strain: Saccharomyces cerevisiae VIN13; Institute of Wine Biotechnology, Stellenbosch University, South Africa
culture: reactor, Bio 110 fermenters (New Brunswick Scientific); Vol, 0.65 L; pH, 5.5 (+/- 1.1, 1 M NaOH);  Air, 0.5128205 vvm (20 L/h); T, 30 °C; DO, >60 %
medium: glucose, 1 or 2 mM; as Verduyn et al. (1992), 1 L


Fig 2 - mu vs. Glc

tool - engauge, R to exchange x and y
file - snoep09_fig2.csv
x - residual glucose, mM
y - growth rate mu = dilution rate, 1/h

NOTE: curve fit to Monod model: K_S = 0.12 mM (asymptotic SEM 0.009)
and mu_max = 0.50 h-1 (asymptotic SEM 0.011).

NOTE: assuming 2 mM glucose in feed, since residual can not be more!

in R:
dat <- read.csv("originalData/snoep09_fig2.csv");write.csv(dat[,2:1],"originalData/snoep09_fig2_processed.csv",row.names=F)

Fig 3 - qGlc vs. D

tool - engauge
file - snoep09_fig3.csv
x - dilution rate, 1h
y - qGlc, mmol(OD_600*h)



