
strain, culture, medium: all data from Postma et al. 1989

NOTE: Postma et al. 1989, Fig.1  only provides Yield but not
biomass and residual glucose; Frede Lei (email) could not
retrieve the data or answer how they obtained biomass and residual glucose;
Postma et al. 1989b, Tab. 1 provides residual glucose for this data, but
for only few points and not consistent with the data here,
eg. at D=0.41/h, Glc in Tab 1 was 1.4 mM, and (1.4/1e3)*180 ~ 0.25 g/L,
while Fig 3 in Lei et al. 2001 gives ca. 0.1 g/L.

Dry weight and glucose concentrations are given for  D 0.36, 0.38 and
0.39 /h in Fig 4 of postma89, so perhaps these values were used
to infer??

Fig.3 - glucose

file - lei01_fig3_glc.dat
x - dilution, 1/h
y - glucose, g/L

11 points

Fig.3 - biomass

file - lei01_fig3_X.dat
x - dilution, 1/h
y - biomass, g/L

17 points

Fig.3 - ethanol

file - lei01_fig3_X.dat
x - dilution, 1/h
y - ethanol, g/L

11 points

Fig.3 - gas

file - lei01_fig3_gas.dat
x - dilution, 1/h
y - qGas, mmol/(g*h)

1:17 - qO2, squares
18:34 - qCO2, inverse triangles

NOTE: edited to align qCO2 and qO2 into file lei01_fig3_gas.csv



Fig.4 - RNA per biomass

tool - engauge
date - 20150630 
file - lei01_fig4_rna.csv
x - dilution, 1/h
y - RNA, % DW, NOTE: y-axis for RNA scaled by 0.04 in paper to give g/g, in engauge scaled by 4 to give % DW

NOTE: data from PhD thesis
Frandsen, S., 1993. Dynamics of Saccharomyces cerevisiae in
continuous culture. Ph.D. thesis, IBT, Technical University
of Denmark.


