
publication: preprint at arxive; 

strain: Saccharomyces cerevisiae DBY12007; diploid; S288c background and WT HAP1originally derived by Hickman and Winston (2007)
culture: reactor, 500 ml Sixfors/Infors AG/Bottmingen/Switzerland; Vol, 0.3 L; Agit, 400 rpm
medium: glucose, 0.3, 0.4, 0.8 or 1.6 g; (NH4)2SO4, 25 g; CaCl2.2H2O, 0.5 g; NaCl, 0.5 g; MgSO4.7H2O, 2.5 g; KH2PO4, 5 g; H3BO3, 0.5 mg; CuSO4.5H2O, 0.04 mg; KI, 0.1 mg; FeCl3.6H2O, 0.2 mg; MnSO4.H2O, 0.4 mg; Na2MoO4.2H2O, 0.2 mg; ZnSO4.7H2O, 0.4 mg; biotin, 2 ug; Ca-pantothenate, 0.4 mg; folic acid, 2 ug; myo-inositol, 2 mg; nicotinic acid, 0.4 mg; p-aminobenzoic acid, 0.2 mg; pyridoxine-HCl, 0.4 mg; riboflavin, 0.2 mg; thiamine-HCl, 0.4 mg



NOTE: medium as in Slavov and Botstein 2011: Saldanha et al. (2004) and Brauer et al. (2005, 2008); personal communication 
see http://web.mit.edu/nslavov/www/PubSlavov/Protocols/glucose_limit.pdf

file - slavov14arxive_fig1c.csv
tool - engauge
x - doublication time, min
y - periods, min

in R, used for copying to periods.gnumeric:
dat <- read.csv("originalData/slavov14arxive_fig1c.csv");dat[] <- dat/60; dat[,1] <- log(2)/dat[,1]
