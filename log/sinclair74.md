
title: Response of mammalian cells to controlled growth rates in steady-state continuous culture
author: R. Sinclair
journal: In Vitro. 1974 Nov-Dec;10(5-6):295-305.
doi: 10.1007/BF02615311. 
pmid:  4616003
