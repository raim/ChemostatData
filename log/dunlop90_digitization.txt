
strain: S. cerevisiae (Anheuser Busch Co., St. Louis, MO)
culture: Vol, 3 L; pH, 3.3; T, 30 °C; Agit, 120 rpm; Air, 3.3 vvm; DO, >6 %; reactor, Virtis fermentor model 43-100
medium: glucose, 11 g; (NH4)Cl, 7.63 g; KH2PO4, 2.81 g; MgSO4.7H2O, 0.59 g; CaCl2.2H2O, 55 mg; FeCl3.6H2O, 37.5 mg; MnSO4.3H2O, 17 mg; ZnSO4.7H2O, 22 mg; CuSO4.5H2O, 4 mg; CoCl2.2H2O, 2.80 mg; Na2MoO4.2H2O, 2.60 mg; H3BO3, 4 mg; KI, 0.60 mg; biotin, 50 ug; thiamine-HCl, 5 mg; myo-inositol, 47 mg; pantothenic acid, 23 mg; pyridoxine, 1.20 mg; EDTA, 0.45 g; antifoam PPG, 40 mg

NOTE: medium developed by Egli and Fiechter: 1981
Medium E (constructed as a consequence of the present study):
carbon up to 4.55 g; (NH4)Cl, 7.63 g; KH2PO4, 2.81 g; MgSO4.7H2O, 0.59 g; CaCl2.2H2O, 55 mg; FeCl3.6H2O, 37.5 mg; MnSO4.3H2O, 17 mg; ZnSO4.7H2O, 22 mg; CuSO4.5H2O, 4 mg; CoCl2.2H2O, 2.80 mg; Na2MoO4.2H2O, 2.60 mg; H3BO3, 4 mg; KI, 0.60 mg; biotin, 50 ug; thiamine-HCl, 5 mg; myo-inositol, 47 mg; pantothenic acid, 23 mg; pyridoxine, 1.20 mg; EDTA, 0.45 g; antifoam PPG, 40 mg


Fig 5&6: dilution vs. yield at different mixing 

tool - engauge, aligned dilution rates in gnumeric
date - 20150604
file - dunlop00_fig6_processed.csv
x - dilution rate, 1/h
y - yield, g DW/g glucose; for differenct mixing microscale

y1: 25 um
y2: 50 um
y3: 130 um
y4: 260 um
