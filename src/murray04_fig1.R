
## growth rate in Murray et al. 2004 - Figure 1
## original data for Figure 1 supplied by Douglas B. Murray (20230327)

gpath <- "~/work/ChemostatData"
dat <- read.csv(file.path(gpath, "originalData",
                          "murray04_fig1_Glucose-Media-Batch.csv"))

## get detailed reactor time series
reactor <- dat[,1:which(colnames(dat)=="DOT...")]

## processed data in right column
cols <- (which(colnames(dat)=="Glycogen")-1):which(colnames(dat)=="Ethanolr")
dat <- dat[,cols]
dat <- dat[!is.na(dat$Cell.number),]

## time in hours! (per comparison with plot in the paper)
time <- dat[,"X.13"]
cells <- dat[,"Cell.number"]/1e8



## calculate growth rate
library(dpseg)

## first growth phase
sg0 <- dpseg(x=time, y=log(cells), P=.1)
plot(sg0, xlab="time/h", ylab=expression(biomass/(log(cells))), vlines=FALSE,delog=TRUE)
sg0

sg1 <- dpseg(x=time, y=log(cells), P=.02)
plot(sg1, xlab="time/h", ylab=expression(biomass/(log(cells))), delog=TRUE)
sg1

## 
sg2 <- dpseg(x=time, y=log(cells), P=.01)
plot(sg2, xlab="time/h", ylab=expression(biomass/(log(cells))), delog=TRUE)
sg2

## diauxic phase
sg3 <- dpseg(x=time, y=log(cells), P=.0001)
sg3

png("murray04_fig1_dpseg.png", units="in", width=6, height=9, res=300)
par(mfcol=c(4,1), mai=c(0,.5,0,.5), mgp=c(1.3,.3,0), tcl=-.25,
    cex=1)
plot(sg0, xlab="time/h", ylab=expression(biomass/(log(cells))), delog=FALSE)
plot(sg1, xlab="time/h", ylab=expression(biomass/(log(cells))), delog=FALSE)
plot(sg2, xlab="time/h", ylab=expression(biomass/(log(cells))), delog=FALSE)
plot(sg3, xlab="time/h", ylab=expression(biomass/(log(cells))), delog=FALSE)
dev.off()

## SPLICE BEST SEGMENTS
sgx <- sg0
sgx$segments <- rbind(sg1$segments[1:2,],
                      sg3$segments[4:6,])

## max growth rate >= 0.4/h
## compare with reactor dynamics

xlm <- range(time) + c(-2,+2)

png("murray04_fig1_reconstructed.png", units="in", width=6, height=9, res=300)
par(mfcol=c(4,1), mai=c(0,.5,0,.5), mgp=c(1.3,.3,0), tcl=-.25,
    cex=1)
plot(time, cells, xlab="time/h", xlim=xlm, main="", col=1, 
     ylab=expression(biomass/(10^8~cells/mL)), log="y")
## select best fits
## first segment from sg0

## plot spliced segments
sgs <- sgx$segments
for ( i in 1:nrow(sgs) ) {
    x <- seq(sgs[i,1],sgs[i,2], .1)
    lines(x=x, y=exp(sgs[i,"intercept"] + sgs[i,"slope"]*x), col=i, lwd=2)
}
legend("bottomright",title=expression(mu/h^-1*"="),
       paste0(round(sgs[,"slope"],2)), bty="n", lty=1, lwd=2, col=1:nrow(sgs))

mtext("time since inoculation / h", 1, 1.3, adj=0.9, cex=1.2)
plot(time, dat$Glucoser*6, xlim=xlm, type="b", ylab="carbon/C-mMol")
lines(time, dat$Ethanolr*2, type="b", pch=4, col=2)
lines(time, dat$Acetaldehyder*2, type="b", pch=3, col=4)
legend("left",c("glucose","ethanol","acetaldehyde"), pch=c(1,4,3,2),
       col=c(1,2,4,3), bty="n")
plot(time, dat$Glycogen, xlim=xlm, type="b",
     ylab="glycogen/(units unknown)")
par(new=TRUE)
plot(time, dat$Trehaloser, xlim=xlm, col=3, pch=2, type="b",
     xlab=NA, ylab=NA, axes=FALSE)
mtext(expression(trehalose/(mu*g/10^6~cells)), 4, 1.3, col=3)
axis(4, col=3, col.axis=3)
legend("left", c("glycogen","trehalose"), col=c(1,3), pch=c(1,2),
       bty="n")
## gasses
plot(reactor[,"X"],reactor[,"DOT..."], xlim=xlm, type="l",
     ylab=expression(dissolved~O[2]/"%"))
par(new=TRUE)
plot(reactor[,"X"],reactor[,"CO2...."], xlim=xlm, type="l",
     col=2,xlab=NA, ylab=NA, axes=FALSE)
mtext(expression(offgas~CO[2]/"%"), 4, 1.3, col=2)
axis(4, col=2, col.axis=2)
dev.off()


