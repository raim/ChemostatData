
### TODO: base pwmavg.R and pwmode.R on 
### new genomeBrowser feature file. 

## generate data figures
$CHEMOSTAT/src/collectData.R --skip.sets=""
$CHEMOSTAT/src/collectPeriods.R

### PWM MODEL

## PWM steady state model: periods and
## growth rate-dependent transcriptome/proteome prediction
R --vanilla < $CHEMOSTAT/src/pwmavg.R
## scale down transcription rate
##sed 's/^scale.trans.*/scale.transcription=TRUE/' $CHEMOSTAT/src/pwmavg.R | R --vanilla -
## variable HOC phase
##sed 's/^thoc.var.*/thoc.var=TRUE/' $CHEMOSTAT/src/pwmavg.R | R --vanilla -
## use Pelechano et al. transcription rates
##sed 's/^use.pelechano.*/use.pelechano10=TRUE/' $CHEMOSTAT/src/pwmavg.R | R --vanilla -
## scaling and var. HOC phase
##sed 's/^thoc.var.*/thoc.var=TRUE/;s/^scale.trans.*/scale.transcription=TRUE/' $CHEMOSTAT/src/pwmavg.R | R --vanilla -
## BEST: var HOC and Pelechano 
sed 's/^thoc.var.*/thoc.var=TRUE/;s/^use.pelechano.*/use.pelechano10=TRUE/' $CHEMOSTAT/src/pwmavg.R | R --vanilla -

## PWM ODE model - reads data from pwmavg.R! 
R --vanilla < $CHEMOSTAT/src/pwmode.R

