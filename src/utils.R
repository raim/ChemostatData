
## plot utilities for growthrate and period data

## plot mode model as ranges
plot_ij <- function(mu, i=1, j=3, lines=TRUE, annotate=TRUE, frequency=FALSE) {

    if ( length(i)>1 | length(j)>1 )
        stop("only single values allowed for i and j")
    
    ## cut Inf ranges
    mus <- mu[mu>0.001] 

    ## get mode
    ij <- getPeriodIJ(mus,i,j)
    i1 <- log(2)/mus # == getPeriodIJ(mus,1,1)


    if ( frequency )  {
        ij <- 1/ij
        i1 <- 1/i1
    }

    ## polygon
    xy <- data.frame(x=c(mus,rev(mus)), y=c(i1, rev(ij)))
    xy <- xy[!is.na(xy$y),]
    
    polygon(x=xy$x, y=xy$y, col=paste0("#CCCCCC","ff"), border=NA)
    if ( lines ) {
        lines(mus,i1, col="#999999", lty=1, lwd=1)
        lines(mus,ij, col="#999999", lty=2, lwd=1)
    }
    if ( annotate ) {
        text(0.3,3, "CDC range", font=2)
        arrows(x0=0.26, y0=3, y1=2.4, lwd=2, length=.05)
    }
}


### TEST FUNCTION; only use in interactive mode
## plot population balance models
test <- FALSE
if ( interactive() & test ) {

  T2test <- seq(0.1,30,0.1) # doubling time range

  plot(0,col=NA,xlab="doubling time, h",ylab="period, h",axes=T,ylim=c(0,12),xlim=c(0,30), yaxs="i", xaxs="i",log="");axis(1);axis(2)
  ## plot modes ij
  for ( i in 1:3 ) 
    for ( j in i:5 ) 
      lines(T2test, getPeriodIJ(D=log(2)/T2test, i, j), col=i)
  ## plot duboc2000, p-lines
  for ( pr in seq(0.1,1,.1) )
    lines(T2test,period_cdc(pr,mu=log(2)/T2test),lty=2)
}

### PLOT PERIODS
plotPeriods <- function(x="D", data,
                        refids, strids, expids, oscids,
                        cols, pchs,
                        prb, modes, moderange,
                        pch.set=c(15:20,2:5), lwd=1, lty=1, cex=1,
                        modecol=c(1:20), modelty=1:6, modelwd=.5,
                        dlim=c(0,0.5),t2lim=c(0,25),plim=c(0,12),flim=c(0,1.5),
                        log="",add=FALSE,legend=FALSE, leg.cex=.5,
                        freq=FALSE, verbose=FALSE, xlab, xpd=FALSE, ...) {

    ddat <- seq(dlim[1],dlim[2],length.out=100)
    tdat <- seq(t2lim[1],t2lim[2],length.out=100)
    if ( x=="D" ) { ## dilution rate
        xlim <- dlim
        xdat <- ddat
        tdat <- log(2)/ddat
        if ( missing(xlab) ) 
            xlab <- expression("dilution rate"~phi/h^-1)
    } else { ## doubling time
        xlim <- t2lim
        xdat <- tdat
        ddat <- log(2)/tdat
        if ( missing(xlab) ) 
            xlab <- expression("culture doubling time"~tau[2]/h)
    }
    if ( freq ) {
        ylab <- expression("frequency"~f[osc]/h^-1)
        ylim <- flim
    } else {
        ylab <- expression("period"~tau[osc]/h)
        ylim <- plim
    }

    ## start empty plot
    if ( !add ) {
        plot(0,col=NA,log=log,xlab=xlab,ylab=ylab,axes=F,
             xlim=xlim,ylim=ylim, yaxs="i", xaxs="i",bty="L")
        axis(1)
        axis(2, at=0:200);axis(4, at=0:200, labels=NA)
    }
    
    ## Duboc and Stockar 2000 - get period for prob. of parent budding
    if ( !missing(prb) ) 
        plotModes(prb=prb, D=ddat, freq=freq,X=x,lwd=modelwd, ...)
    
    ## Bellgardt 1994a - get mode lines
    if ( !missing(moderange) & x=="D" )
        plot_ij(mu=ddat,
                i=moderange["i"],j=moderange["j"], lines=FALSE, annotate=FALSE)
    if ( !missing(modes) ) {
        tmpcol <- col2rgb(modecol)/255
        for ( k in 1:ncol(tmpcol) )
            modecol[k] <- rgb(tmpcol[1,k],tmpcol[2,k],tmpcol[3,k],250/255)
        plotModes(modes, D=ddat, modecol=modecol,
                  modelty=modelty,freq=freq,X=x,lwd=modelwd,...)
    }
        
    
    ## return if no data is available
    if ( missing(data) ) { if(verbose) cat(paste("no data\n"));return();}

    ## filter available data
    miss <- NULL
    if ( !missing(refids) )
        if ( sum(refids%in%data[,"refID"])<length(refids) ) 
            miss <- c(miss,refids[!refids%in%data[,"refID"]])
    if ( !missing(expids) )
    if ( sum(expids%in%data[,"expID"])<length(expids) ) 
        miss <- c(miss,expids[!refids%in%data[,"expID"]])
    if ( !missing(oscids) )
        if ( sum(oscids%in%data[,"oscID"])<length(oscids) ) 
            miss <- c(miss,oscids[!refids%in%data[,"oscID"]])
    if ( !missing(strids) )
        if ( sum(strids%in%data[,"strain"])<length(strids) ) 
            miss <- c(miss,strids[!strids%in%data[,"strain"]])
    if ( !is.null(miss) )
        cat(paste("WARNING: couldn't find", paste(miss,collapse=";"), "\n"))
    
  
    ## collect all requested data
    if ( missing(oscids) ) oscids <- NULL
    if ( !missing(refids) ) 
        oscids <- c(oscids,
                    as.character(data[data[,"refID"]%in%refids,"oscID"]))
    if ( !missing(expids) )
        oscids <- c(oscids,
                    as.character(data[data[,"expID"]%in%expids,"oscID"]))
    if ( !missing(strids) )
        oscids <- c(oscids,
                    as.character(data[data[,"strain"]%in%strids,"oscID"]))
    oscids <- unique(oscids)
    expids <-  unique(as.character(data[data[,"oscID"]%in%oscids,"expID"]))

    ## setup colors and pch
    if ( missing(cols) ) {
        cols <- 1:length(oscids)
        names(cols) <- oscids
    } else if ( length(cols)==1 ) {
      cols <- rep(cols, length(oscids))
      names(cols) <- oscids
    }
    if ( missing(pchs) ) {
        pchs <- rep(16,length(oscids))
        names(pchs) <- oscids
    }
    
    ## PLOT BY EXPERIMENTS (connected by lines)
    leg.lty <- leg.pch <- leg.nam <- leg.col <- NULL
    for ( exp in expids ) {
    
        if ( verbose ) cat(paste("plotting", exp, "\n"))

        ## select data
        set <- data[,"expID"]==exp & data[,"oscID"]%in%oscids
        dat <- data[set,,drop=FALSE]
        dat <- dat[order(as.numeric(dat[,x])),,drop=FALSE]
        
        ## skip if no data is available
        if ( sum(is.na(dat[,1]))==nrow(dat) ) {
            ##cat(paste("NO DATA",exp, paste(oscs,collapse=";"),"\n"))
            next
        }
        
        ## plot styles
        oscs <- as.character(dat[!is.na(dat[,1]),"oscID"]) # only plotted
        col <- cols[oscs]
        pch <- pchs[oscs]
        cx <- rep(cex,length(oscs))
        cx[pch%in%c(3:4)] <- cex*.6
        pw <- rep(1,length(oscs))
        pw[pch%in%c(3:4)] <- 2 # 3.3
        ## store for legend
        leg.lty <- c(leg.lty, c(1,rep(0,length(oscs)-1)))
        leg.pch <- c(leg.pch, pch)
        leg.nam <- c(leg.nam, oscs)
        leg.col <- c(leg.col, col)
        
        ## plot data
        dat <- cbind(as.numeric(dat[,x]),as.numeric(dat[,"P"]))
        if ( freq ) dat[,2] <- 1/dat[,2]
        
        
        
        if ( nrow(dat)>1 & !is.null(lwd) ) 
            lines(dat[,1:2,drop=FALSE],col=col,type="l",lwd=lwd,lty=lty)
        points(dat[,1:2,drop=FALSE], col=col, pch=pch, cex=cx, lwd=pw, xpd=xpd)
    }
    ## construct legend
    ## expids with line, then oscids with pch
    if ( legend )
        legend(ifelse(x=="D","topright","bottomright"),
               legend=leg.nam, lty=leg.lty, col=leg.col, pch=leg.pch,
               bty="n", pt.cex=leg.cex, cex=.8*cex, y.intersp=.8)

    ## silently return plotted experiments
    invisible(leg.nam)
}

plotModes <- function(modes, prb, D, modecol=c(1:20), modelty=1:6,
                      freq=FALSE,X="D", add.label=TRUE, label.sides=3:4, ...) {
    ## only above 0!
    D <- D[D>0]
    xdat <- D
    if ( X!="D" ) xdat <- log(2)/xdat
    if ( !missing(prb) ) {
        for ( pr in prb ) {
            ydat <- period_cdc(pr, mu=D, freq=freq)
            lines(xdat, ydat, lty=1,col="gray",...)
            ## label axis by probability
            if ( add.label ) {
                if ( 4 %in% label.sides ) {
                    ax4 <- ifelse(X=="D", par("usr")[2], log(2)/par("usr")[2])
                    ax4 <- period_cdc(pr, mu=ax4, freq=freq)
                    axis(4, at=ax4,label=pr,las=2,
                         col.axis="gray",col="gray",tcl=0,
                         mgp=c(0,0,0),cex.axis=.7)
                }
                if ( 3 %in% label.sides ) {
                    ax3 <- ifelse(freq, 1/par("usr")[4], par("usr")[4])
                    ax3 <- period_cdc(pr, mu=ax3)
                    ax3 <- ifelse(X=="D", ax3, log(2)/ax3)
                    axis(3, at=ax3,label=pr,las=1,
                         col.axis="gray",col="gray",tcl=0,
                         mgp=c(0,0,0),cex.axis=.7)
                }
            }
        }
    }
    if ( !missing(modes) ) {
        modes <- strsplit(modes,":")
        for ( mode in modes ) {
            ##cat(paste("plotting mode", mode, "\n"))
            ij <- as.numeric(mode)
            ydat <- getPeriodIJ(D=D,i=ij[1],j=ij[2], freq=freq)
            col <- modecol[(ij[1]-1)%%length(modecol) +1]
            lty <- modelty[(ij[2]-1)%%length(modelty) +1]
            lines(xdat,ydat,col=col,lty=lty,...)
            ## label axis by mode lines
            ## get cut points with upper x and y lims and use for axis
            if ( add.label ) {
                if ( 4 %in% label.sides ) {
                    ax4 <- ifelse(X=="D", par("usr")[2], log(2)/par("usr")[2])
                    ax4 <- getPeriodIJ(D=ax4,i=ij[1],j=ij[2], freq=freq)
                    axis(4, at=ax4,label=paste(mode,collapse=":"),las=2,
                         col.axis=col,col=col,
                         tcl=0,mgp=c(0,0,0),cex.axis=.7)
                }
                if ( 3 %in% label.sides ) {
                    ax3 <- ifelse(freq, 1/par("usr")[4], par("usr")[4])
                    ax3 <- getPeriodIJ(D=ax3,i=ij[1],j=ij[2])
                    ax3 <- ifelse(X=="D", ax3, log(2)/ax3)
                    axis(3, at=ax3,label=paste(mode,collapse=":"),
                         las=2,col.axis=col,col=col,
                         tcl=0,mgp=c(0,0,0),cex.axis=.7)
                }
            }
        }
    }
}


## PARSE MEDIUM STRING
## converts all substrate units
## % -> g (1 % is 10 g of 1000 g)
## g -> mol (if MW is defined)
## mmol -> mol
## mol -> C-mMol (if CM is defined)
parseMedium <- function(mediumString, debug=FALSE, verbose=FALSE) {
  ## NOTE: requires parameters from utils/parameters.R
  substrate <- NULL
  if ( mediumString!="" ) {
    medium <- lapply(strsplit(unlist(strsplit(mediumString,";")),","),trim)
    substrate <- data.frame(matrix("",nrow=0,ncol=7))
    for ( j in 1:length(medium) ) {
      subid <- medium[[j]][1]
      tmp <- unlist(strsplit(medium[[j]][2]," "))
      amount <- as.numeric(tmp[1])
      unit <- trim(ifelse(length(tmp)==2,tmp[2],""))
      if ( is.na(amount) )
        cat(paste("medium", j, subid, ": no amount provided\n"))

      ## account for % indications (e.g. H2SO4 (70%))
      if ( length(grep("%", subid))>0 ) {
        pat <- "(.*)\\(([0-9]+)%\\)";
        prc <- as.numeric(sub(pat, "\\2", subid))

        if ( debug ) cat(paste(prc, "percent of", subid, amount, " "))
        amount <- prc*amount/100
        subid <- trim(sub(pat, "\\1", subid))
        if ( debug ) cat(paste("is", subid, amount, unit, "\n"))
      }

      ## convert units and calculate compositions
      cnv <- convertUnits(amount=amount, id=subid, unit=unit,
                          MW=MW, CM=CM, CC=CC, DNS=DNS)
      if ( debug | (verbose & !is.null(cnv$convert)) )
        cat(paste("converted", subid, unit, "to", cnv$unit, "\n"))
      amount <- cnv$amount
      unit <- cnv$unit
      
      substrate <-rbind(substrate,
                        data.frame(subid, amount, unit))
    }
    rownames(substrate) <- substrate[,1]
    substrate <- substrate[,2:ncol(substrate),drop=FALSE]
    substrate <- cbind(substrate, mediumContent(substrate))
  }
  substrate
}

## including gas law!
convertUnits <- function(amount, id, unit, MW, CM, CC, DNS,
                         P=101.325, R=8.314472, T=303.15,
                         dw.percent=FALSE, cdw2cmol=FALSE) {

  convert <- NULL

### (1) AMOUNTS - used by medium parser, where all is defined per L
  ## convert units

  ## convert g from %, restrict to substances with defined MW
  ## TODO: restrict to liquid substance, handle gas separately
  if ( !missing(MW) )
    if ( unit == "%" & id %in% names(MW)) { 
      amount <- amount * 10
      unit <- "g"
      convert <- paste(convert,"% to g",sep=";")
    }
  ## convert ug to g
  if ( unit == "ug" ) { 
    amount <- amount / 1e6
    unit <- "g"
    convert <- paste(convert,"ug to g",sep=";")
  }
  ## convert mg to g
  if ( unit == "mg" ) { 
    amount <- amount / 1e3
    unit <- "g"
    convert <- paste(convert,"mg to g",sep=";")
  }
  ## convert uL to L
  if ( unit %in% c("uL","ul") ) { 
    amount <- amount / 1e6
    unit <- "L"
    convert <- paste(convert,"uL to g",sep=";")
  }
  ## convert mL to L
  if ( unit %in% c("ml","mL") ) { 
    amount <- amount / 1e3
    unit <- "L"
    convert <- paste(convert,"mL to g",sep=";")
  }
  ## convert L to g : follow-up, requires L and density
  if ( !missing(DNS) )
      if ( unit %in% c("L","l") & id %in% names(DNS) ) {
        amount <- amount * DNS[id] # L * g/L
        unit <- "g"
        convert <- paste(convert,"L to g",sep=";")
      }
  ## convert from g to mol - follow up, requires g and MW
  if ( !missing(MW) )
    if ( unit == "g" & id %in% names(MW) ) {
      amount <- amount/MW[id]
      unit <- "mol"
      convert <- paste(convert,"g to mol",sep=";")
    }
  ## convert from mmol to mol
  if ( unit == "mmol" ) {
    amount <- amount/1000
    unit <- "mol"
    convert <- paste(convert,"mmol to mol",sep=";")
  }

### DIVERSE UNITS/DATA: data of growthrates.csv
  
  ## TIMES - e.g. budding times, periods, etc.
  ## min to h
  if ( unit == "min" ) {
    amount <- amount/60
    unit <- "h"
    convert <- paste(convert,"min to h",sep=";")
  }
  if ( unit == "1/min" ) {
    amount <- amount*60
    unit <- "1/h"
    convert <- paste(convert,"1/min to 1/h",sep=";")
  }

  ## CONCENTRATIONS: 1) to g/L; 2) to mmol/L; 3) to C-mmol/L
  ## mg/L to g/L
  if ( unit == "mg/L" ) {
    amount <- amount/1e3
    unit <- "g/L"
    convert <- paste(convert,"mg/L to g/L",sep=";")
  }
  ## g/L to mMol
  if ( !missing(MW) )
    if ( unit == "g/L" & id %in% names(MW) ) {
      amount <- 1e3 * amount/MW[id]
      unit <- "mMol"
      convert <- paste(convert,"g/L to mMol",sep=";")
    }
  ## mg/g to g/g
  if ( unit == "mg/g" ) {
    amount <- amount/1e3
    unit <- "g/g"
    convert <- paste(convert,"mg/g to g/g",sep=";")
  }
  ## g/g to mmol/g
  if ( !missing(MW) )
    if ( unit == "g/g" & id %in% names(MW) ) {
      amount <- 1e3 * amount/MW[id]
      unit <- "mmol/g"
      convert <- paste(convert,"g/g to mmol/g",sep=";")
    }
    ## umol to mmol
    ## NOTE/TODO: check the umol is ^1!!
    if ( length(grep("umol", unit))>0 ) {
        amount <-  amount/1e3 
        unit <- sub("umol","mmol",unit)
        convert <- paste(convert,"umol to mmol",sep=";")
    }
    ## uMol to mMol
    ## NOTE/TODO: check the umol is ^1!!
    if ( length(grep("uMol", unit))>0 ) {
        amount <-  amount/1e3 
        unit <- sub("uMol","mMol",unit)
        convert <- paste(convert,"uMol to mMol",sep=";")
    }

  ## C-MOL - NOTE: follow-up conversion - requires mMol
  ## convert to c-Mol; follow-up from g/L to mMol
    mmol <- c("mMol","mM")
  if ( !missing(CM) )
    if ( unit %in% mmol & id %in% names(CM) ) {
      amount <- amount * CM[id]
      unit <- "C-mMol"
      convert <- paste(convert,"mMol to C-mMol",sep=";")
    }
  
    ## SPECIFIC FLUXES


    
  ## ml to mmol gas - gas law!
  ## TODO: generalize to 'L' and restrict to specified gases
    mlgh <- c("mL/(g*h)","mL/g/h")
  if ( unit %in% mlgh ) {
    amount <- P*amount/(R*T) ## GAS LAW: L -> mol
    unit <- "mmol/g/h"
    convert <- paste(convert,"mL/g/h to mmol/g/h",sep=";")
  }

  ## C-MOL
  ## convert mmol/(g*h) to C-mmol/(g*h)
    mmolgh <- c("mmol/(g*h)","mmol/g/h")
  if ( !missing(CM) )
    if ( unit %in% mmolgh & id %in% names(CM) ) {
      amount <- amount * CM[id] #/ 1e3
      unit <- "C-mmol/g/h"
      convert <- paste(convert,"mmol/g/h to C-mmol/g/h",sep=";")
    }
  ## convert mmol/g to C-mmol/g
    mmolg <- c("mmol/g")
  if ( !missing(CM) )
      if ( unit %in% mmolg & id %in% names(CM) ) {
      amount <- amount * CM[id] #/ 1e3
      unit <- "C-mmol/g"
      convert <- paste(convert,"mmol/g to C-mmol/g",sep=";")
    }

    ## CONVERT DRY WEIGHT from gram to C-mol
    if ( cdw2cmol ) {
        ## convert /(g*h) to /(C-mol*h) - NOTE - always assuming /(g DW * h)
        if ( !missing(MW) & !missing(CC) ) {
            if ( length(grep("/\\(g\\*h\\)$", unit))>0 ) {
                amount <- amount * MW["C"] / CC["DW"]
                unit <- sub("/\\(g\\*h\\)", "/C-mol/h", unit)
                convert <- paste(convert,"/g/h to /C-mol/h",sep=";")
            }
            if ( length(grep("/g/h$", unit))>0 ) {
                amount <- amount * MW["C"] / CC["DW"]
                unit <- sub("/g/h", "/C-mol/h", unit)
                convert <- paste(convert,"/g/h to /C-mol/h",sep=";")
            }
        }
        ## convert /g to /C-mol - NOTE - always assuming /g is /(g DW)
        if ( !missing(MW) & !missing(CC) ) {
            if ( length(grep("/g$", unit))>0 ) {
                amount <- amount * MW["C"] / CC["DW"]
                unit <- sub("/g", "/C-mol", unit)
                convert <- paste(convert,"/g to /C-mol",sep=";")
            }
        }
    }
    if ( dw.percent ) { 
        ## convert "mmol/C-mol" to "% DW-C"
        if ( length(grep("mmol/C-mol", unit))>0 ) {
            amount <- amount / 10
            unit <- sub("mmol/C-mol", "% C", unit)
            convert <- paste(convert,"mmol/C-mol to % C",sep=";")
        }
        ## for shorter unit labels: convert C-% DW-C to % DW-C
        if ( length(grep("C-% C", unit))>0 ) {
            unit <- sub("C-% C", "% C", unit)
            convert <- paste(convert,"C-% DW-C to % C",sep=";")
        }
    }

    ## first, unify equal units
    unit <- sub("\\/g\\/h", "/(g*h)", unit)

    return(list(amount=amount, unit=unit, convert=convert))
}

## calculate C,N,SO4,PO4 content of medium
mediumContent <- function(medium) {
  
  cnsp <- matrix(0, nrow=nrow(medium), ncol=4)
  rownames(cnsp) <- rownames(medium)
  colnames(cnsp) <- paste(c("C","N","S","P"),"-mmol",sep="")
  
  for ( i in 1:nrow(medium) ) {
    subid <- rownames(medium)[i]
    amount <- medium[i,"amount"]
    unit <- medium[i,"unit"]
    if ( unit == "mol" ) {
      if ( subid %in% names(CM) )
        cnsp[i,"C-mmol"] <- 1e3 * amount * CM[subid]
      if ( subid %in% names(NM) )
        cnsp[i,"N-mmol"] <- 1e3 * amount * NM[subid]
      if ( subid %in% names(SM) )
        cnsp[i,"S-mmol"] <- 1e3 * amount * SM[subid]
      if ( subid %in% names(PM) )
        cnsp[i,"P-mmol"] <- 1e3 * amount * PM[subid]
    }
    ## estimated from complex media
    if ( unit == "g" ) {
      if ( subid %in% names(CC) )
        cnsp[i,"C-mmol"] <- 1e3 * amount * CC[subid] / MW["C"] 
      if ( subid %in% names(NC) )
        cnsp[i,"N-mmol"] <- 1e3 * amount * NC[subid] / MW["N"] 
      if ( subid %in% names(SC) )
        cnsp[i,"S-mmol"] <- 1e3 * amount * SC[subid] / MW["SO4"] 
      if ( subid %in% names(PC) )
        cnsp[i,"P-mmol"] <- 1e3 * amount * PC[subid] / MW["PO4"] 
    }
  }
  cnsp
}


### GAS CALCULATIONS
## applies a first-order transport equation to
## back-calculate original data by removing delays
## by probe response time; e.g. useful to estimate
## dissolved gas concentrations from offgas concentrations of
## a bioreactor
rmResponseTime <- function(x, time, tau, span=4) {
    o <- x
    o[] <- NA
    ## manual differentiation
    for ( t in (span+1):(length(time)-span) ) {
        ## average over 2*span steps
        deltaC <- mean(diff(x[(t-span):(t+span)]))
        deltaT <- mean(diff(time[(t-span):(t+span)]))
        o[t] <- x[t] + deltaC/deltaT * tau
    }
    o
}

## new version that uses linear regression to get
## the local slope and can take pre-smoothed data (with NAs)
## TODO: is it better? it is much slower!?
rmResponseTimeLM <- function(x, time, tau, span=4) {
  nas <- is.na(x)
  time <- time[!nas]
  x <- x[!nas]
  o <- x
  o[] <- NA
  ## manual differentiation
  for ( t in (span+1):(length(time)-span) ) {
    ## average over 2*span steps
    deltaC <- x[(t-span):(t+span)]
    deltaT <- time[(t-span):(t+span)]
    fit <- lm(deltaC ~ deltaT)
    
    o[t] <- x[t] + fit$coefficient[[2]] * tau
  }
  o
  new <- rep(NA,length(nas))
  new[!nas] <- o
  new
}

### GENERAL UTILS
## helper get rid of leading and trailing whitespaces
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
## after http://www.magesblog.com/2013/04/how-to-change-alpha-value-of-colours-in.html
add.alphas <- function(col, alpha=rep(1,length(col))){
    lcol <- lapply(col, function(x) c(col2rgb(x)/255))
    sapply(1:length(col), function(x) {
        y <- lcol[[x]]
        rgb(y[1], y[2], y[3], alpha=alpha[x])})  
}
## use plot devices consistently
plotdev <- function(file.name="test", type="png", width=5, height=5, res=100, bg="white") {
  file.name <- paste(file.name, type, sep=".")
  if ( type == "png" )
    png(file.name, width=width, height=height, units="in", res=res, bg=bg)
  if ( type == "eps" )
    postscript(file.name, width=width, height=height, paper="special")
  if ( type == "pdf" )
    pdf(file.name, width=width, height=height,bg=bg)
  if ( type == "svg" )
    svg(file.name, width=width, height=height,bg=bg)
}



### PARSE, PROCESS & PLOT DATA FILES FROM CHEMOSTATDATA

#' parse data from ChemostatData
#' @param files files from ChemostatData collection
#' @param path path to ChemostatData collection
#' @param minus character vector of data IDs for which the
#' value will be multiplied with -1
#' @param convert convert units to internal standards
## NOTE: this is only used in collectOscillations currently
parseData <- function(files, path, minus=NULL, convert=TRUE,
                      dw.percent=TRUE, cdw2cmol=TRUE) {
    data <- list()
    ids <- character()
    names <- character()
    units <- character()
    xax <- NA
    for ( i in 1:length(files) ) {
        dat <- read.csv(file.path(path, files[i]),check.names=FALSE)
        xax <- range(c(xax,dat[,1]),na.rm=TRUE)
        for ( j in 2:ncol(dat) ) {
            dt <- dat[!is.na(dat[,j]),c(1,j)]
            tmp <- unlist(strsplit(colnames(dat)[j],";"))
            id <- name <- tmp[1]
            unit <- NA
            if ( length(tmp)==2 )
                unit <- tmp[2]
            
            ## transform
            if ( id %in% minus ) {
                dt[,2] <- - dt[,2]
                name <- paste("-",name)
            }
            ids <- c(ids, id)
            names <- c(names, name)
            if ( convert & !is.na(unit) ) {
                dc <- convertUnits(dt[,2],
                                   id, unit, MW=MW, CM=CM, CC=CC, DNS=DNS,
                                   dw.percent=dw.percent, cdw2cmol=cdw2cmol)
                dt[,2] <- dc$amount
                unit <- dc$unit
            }
            units <- c(units, unit)
            data <- append(data, list(dt))
       }
    }
    names(data) <- ids
    data$ids <- ids
    names(names) <- ids
    data$names <- names
    names(units) <- ids
    data$units <- units
    data$xax <- xax
    data
}

#' set the time to zero by subtracting first timepoint from all
#' @param data data structure from \code{\link{parseData}}
#' @param t0 new initial time, defaults to 0
dataSetInitialTime <- function(data, t0=0) {
    t0 <- data$xax[1] + t0
    for ( id in data$ids ) 
        data[[id]][,1] <- data[[id]][,1] - t0
    data$xax <- data$xax - t0
    data
}
#' converts all times, default form min to hour
#' @param f conversion factor, e.g. to
#' convert from min to hour f=1/60
dataConvertTime <- function(data, f=1/60) {
    data$xax <- data$xax*f
    for ( id in data$ids ) 
        data[[id]][,1] <- data[[id]][,1]*f
    data    
}


#' interpolate data to common time axis
#' TODO: smart time resolution
#' TODO: use spline fits?
#' @param data data structure from \code{\link{parseData}}
#' @param ids subset of data IDs to interpolate
#' @param n number of intervals, see \code{\link[]{approx}}
dataInterpolate <- function(data, ids, n=50) {
    if ( missing(ids) ) ids <- data$ids
    x.out <- seq(data$xax[1],data$xax[2],diff(data$xax)/n)
    for ( id in ids ) {
        xy <- approx(x=data[[id]][,1], y=data[[id]][,2], n=n, rule=2)
        tmp <- cbind(xy$x,xy$y)
        colnames(tmp) <- colnames(data[[id]])
        data[[id]] <- tmp
    }
    data
}

#' @param data data structure from \code{\link{parseData}}
#' @param xy new time series to add
#' @param id ID for the new data
#' @param name optional name (used in plot axes); defaults to \code{id}
#' @param unit optional unit
addData <- function(data, xy, id, name, unit=NA) {
    
    if ( id %in% data$ids ) { # replace existing
        cat(paste(id, "exists! Replacing\n"))
        new <- data
        new[[id]] <- xy
        if ( !missing(name) )
            new$names[id] <- name
        if ( !is.na(unit) )
            new$units[id] <- unit
    } else { # add new
        new <- append(data, list(xy))
        names(new) <- c(names(data), id)
        new$ids <- c(data$ids,id)
        new$names <- c(data$names,ifelse(missing(name),id,name))
        new$units <- c(data$units,unit)
        names(new$names) <- names(new$units) <- new$ids
    }
    new$xax <- range(c(data$xax,xy[,1]),na.rm=TRUE)
    new
}

#' plot data from ChemostatData
#' @param data data structure from \code{\link{parseData}}
#' @param ids subset of data IDs to plot; also sets the y-axes order
#' @param ref reference data ID, will be plotted wit special style
#' in the background
#' @param xlim set xlim; if not specified the global range in \code{data}
#' will be used
#' @param xlab label for x-axis
#' @param ylims a single \code{ylim} for all or a named list of
#' ylims for individual data IDs
#' @param col a vector of colors for ids; the vector can be named by
#' data \code{ids}, otherwise it will be used in order
#' @param type as \code{col} but for plot types ("l", "p", "b")
#' @param pch as \code{col} but for point types
#' @param lty as \code{col} but for line types
#' @param axis.col color for all y-axes; if not specified the same
#' color will be used as for the data plots
## @param add logical value: add to existing plot if TRUE; NOTE, that this
## is distinct from conventional \code{add}, it will use \code{par(new=TRUE)}
## and thus reset the axes; user needs to take care about proper x-axis
## alignment
#' @param ... arguments to plot
plotData <- function(data, ids, ref, xlab="NA", xlim, lwd=2, axis.col,
                     ylabs, ylims=NULL,
                     col=NULL, type=NULL, pch=NULL, lty=1, 
                     mar.bottom=2, mar.top=.5, ...) {

    ## data and data ranges
    ## x-axis
    xax <- data$xax
    if ( missing(xlim) ) xlim <- range(data$xax)
    ## y-axis
    if ( missing(ids) ) ids <- data$ids
    N <- length(ids)
    if ( missing(ylabs) ) {
        ylabs <- data$names
        ylabs[!is.na(data$units)] <-
            paste(data$names,data$units, sep="/")[!is.na(data$units)]
    }
    
    ## plot styles for named lists (incomplete data allowed), incl ylims!
    ## first, catch single unnamed to be expanded
    if ( length(ylims)==2 & is.null(names(ylims)) ) {
        ylims <- rep(list(ylims), N)
        names(ylims) <- ids
    }
    if ( length(type)==1 & is.null(names(type)) ) {
        type <- rep(type, N)
        names(type) <- ids
    }
    if ( length(col)==1 & is.null(names(col)) ) {
        col <- rep(col, N)
        names(col) <- ids
    }
    if ( length(pch)==1 & is.null(names(pch)) ) {
        pch <- rep(pch, N)
        names(pch) <- ids
    }
    if ( length(lty)==1 & is.null(names(lty)) ) {
        lty <- rep(lty, N)
        names(lty) <- ids
    }

    ## partial lists: expand with defaults
    for ( i in 1:N ) {
        id <- ids[i]
        if ( !id %in% names(ylims) ) {
            tmp <- c(NA,NA)
            if ( id %in% data$ids )
                tmp <- list(range(data[[id]][,2]))
            names(tmp) <- id
            ylims <- append(ylims, tmp)
        }
        if ( !id %in% names(type) ) {
            type <- c(type, "b")
            names(type)[length(type)] <- id
        }
        if ( !id %in% names(col) ) {
            col <- c(col, rainbow(N)[i])
            names(col)[length(col)] <- id
        }
        if ( !id %in% names(lty) ) {
            tmp <- 1 # rep(1:6, len=N)[i] # avoid multiples of length(pch)
            lty <- c(lty, tmp)
            names(lty)[length(lty)] <- id
        }
        if ( !id %in% names(pch) ) {
            tmp <- rep(1:6, len=N)[i] # avoid multiples of length(lty)
            pch <- c(pch, tmp)
            names(pch)[length(pch)] <- id
        }
    }

    ## plot setup
    ## TODO: read mgp an mar from environment
    ## and multiply accordingly
    numax <- floor(N/2)
    odd <- N%%2 == 1
    mar <- c(mar.bottom,
             2.5*(numax+odd),
             mar.top,
             2.5*numax)+.1
    
    ## plot
    par(mar=mar, mgp=c(1,.2,0), tcl=-.2)
    plot(1,col=NA,xlab=NA,ylab=NA,axes=FALSE,xlim=xlim,...)
    axis(1)
    mtext(xlab, 1, 1.1)
    ## reference data
    if ( !missing(ref) ) {
        id <- ref
        par(new=TRUE)
        ylim <- ylims[[id]]
        yrng <- pretty(data[[id]][,2])
        dt <- data[[id]]
        cl <- add.alphas(col[[id]],.5)
        
        plot(dt, xlab=NA,ylab=NA,axes=FALSE, xlim=xlim, ylim=ylim, 
             col=NA, type=type[id], lwd=lwd, pch=pch[id], lty=lty[id],...)
        polygon(x=c(dt[1,1],dt[,1],dt[nrow(dt),1]),
                y=c(ylim[1],data[[id]][,2],ylim[1]),
                col=cl,border=NA)
    }
    ## all data
    for ( i in 1:N ) {
        id <- ids[i]
        if ( !id %in% data$ids ) next
        if ( id=="" ) next
        par(new=TRUE)
        ylim <- ylims[[id]]
        yrng <- pretty(data[[id]][,2])
        plot(data[[id]], xlab=NA,ylab=NA,axes=FALSE, xlim=xlim, ylim=ylim, 
             type=type[id], lwd=lwd, pch=pch[id], col=col[id], lty=lty[id],...)
        # axis side: odd->left; even->right
        side <- 2*(2 - i%%2)
        # axis line:
        line <- (i*1 - (i%%2==0) -1)+.1
        if ( !missing(axis.col) ) ax.col <- axis.col
        else ax.col <- col[id]
        axis(side=side, line=line,col.axis=ax.col, col=ax.col, at=yrng)
        mtext(ylabs[id], at=mean(yrng), side=side, line=line+1, col=ax.col)
        #cat(paste(i, id, line, "\n"))
    }
    ## return plot styles, e.g. for legend
    ids <- ids[ids %in% data$ids]
    styles <- data.frame(type=type[ids],
                         col=col[ids],
                         lty=lty[ids],
                         pch=pch[ids])
    styles[styles[,"type"]=="l","pch"] <- NA
    styles[styles[,"type"]=="p","lty"] <- NA
    invisible(styles)
}
