
setwd("/home/raim/work/ChemostatData/originalData")
sdat <- read.csv("forquet21_fig3A_Senterica.csv")
edat <- read.csv("forquet21_fig3A_Ecoli.csv")


png("forquet21_fig3A_digitized.png", width=5, height=5, units="in", res=200)
par(mfcol=c(2,1), mai=c(.5,.5,.1,.1))
matplot(sdat[,1], sdat[,2:4], col=c(1,2,4), lty=1, type="l", lwd=2)
legend("topleft", colnames(sdat)[2:4], col=c(1,2,4), lty=1, lwd=2)
legend("topright", "S. enterica", bty="n")
matplot(edat[,1], edat[,2:4], col=c(1,2,4), lty=1, type="l", lwd=2)
legend("topleft", colnames(edat)[2:4], col=c(1,2,4), lty=1, lwd=2)
legend("topright", "E. coli", bty="n")
dev.off()
