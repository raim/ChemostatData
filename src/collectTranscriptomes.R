
## TODOs:
## * make independent of complete.RData
## * load cluster info from a file in /data/yeast or /data/genomeData/yeast
## * average expression over one cycle <-> growth rate? <-> slavov model

### LOAD DATA SET FROM MACHNE & MURRAY 2012
## TODO: make independent of that!!

## location of former tataProject folders originalData/ & processedData/
yeast.path <- sub("YEASTDAT=","",system("env|grep YEASTDAT",intern=TRUE))


## load clustering
## TODO: generate a simple globally accessible R file that contains
## all clustering data relevant for plotting:
## coarse.col, coarse.srt(.major/minor)
#dat <- read.csv("~/public_html/data/2011/yeast/clusters/supplementaryDatasets/tuliCoarse.results.csv")
## load clustering - full data clusterFigures??.R
## TODO: get locally 
load(file.path(yeast.path,"processedData","clusterFigures2011.store.complete.RData"))
#intron:
#load("/home/raim/data/2011/yeast/next/clusterFigures2011.store.complete.RData")

## remove obsolete functions in RData
rm("plotdev")

coarse.srt.ad <- c("A","D") # 
coarse.srt.acd <- c("D","C","A") # 
coarse.srt.ab <- c("AB","A") # 
coarse.srt.bcd <- c("B","B.C","B.D") # 
coarse.srt.abcd <- rev(coarse.srt.major)

## expand colors
coarse.col.all <- coarse.col[coarse.cls]
coarse.col.major <- coarse.col.all
coarse.col.major[!coarse.cls%in%coarse.srt.major] <- NA

## generate cset to use with segmenTools functions
## NOTE: requires numeric cluster labels
coarse.num <- 1:length(coarse.srt)
names(coarse.num) <- coarse.srt
cols <- coarse.col[coarse.srt]
names(cols) <- coarse.num
clusters <- cbind("K:14"=coarse.num[coarse.cls])
rownames(clusters) <- names(coarse.cls)
cset <- list(clusters=clusters,
             colors=list("K:14"=cols),
             sorting=list("K:14"=coarse.num),
             selected="14")
class(cset) <- "clustering"

### RESULT COLLECTION
## average gene expression
genes.avgl <- list()

### LOAD ALL OTHER STUFF (from diverse repositories)

library(segmenTools) # cluster plot
library(segmenTier) # dft and clustering

## genomeBrowser data
data.path <- sub("GENDAT=","",system("env|grep GENDAT",intern=TRUE))

## CHEMOSTATDATA
chemo.path <- "~/work/ChemostatData/"
chemo.data <- file.path(chemo.path,"originalData")

## local yeast data
yeast.path <- sub("YEASTDAT=","",system("env|grep YEASTDAT",intern=TRUE))

## genomeBrowser data
data.path <- sub("GENDAT=","",system("env|grep GENDAT",intern=TRUE))

### OUTPUT:
out.path <- "~/work/ChemostatData/"
fig.path <- "~/work/ChemostatData/figures/transcriptomes"
dir.create(fig.path)
fig.type <- "pdf" # "png" # 



## LOCAL HELPER FUNCTIONS

name2id <- function(x) {
  names <- x
  for ( i in 1:length(x) )
    if ( x[i] %in% genes[,"name"] ) 
      names[i] <- as.character(genes[genes[,"name"]==x[i],"ID"])
  names
}
id2name <- function(x) {
  id <- x
  for ( i in 1:length(x) )
    id[i] <- as.character(genes[genes[,"ID"]==x[i],"name"])
  id[id==""] <- x[id==""]
  id
}

## phase-ordered genes
plotGenes <- function(data, cset, times, cycles=3, file.name) {

    phase <- calculatePhase(data, cycles=cycles)
    cnrm <- data[order(phase),]
    cnrm <- t(apply(cnrm, 1, function(x) (x-min(x))/(max(x)-min(x))))

    cset$clusters <- cset$clusters[order(phase),,drop=FALSE]

    if ( !missing(file.name) ) 
        plotdev(file.name, type=fig.type, width=5,height=8)
    layout(t(1:2),widths=c(.1,.9))
    par(mai=c(.5,0,.1,0))
    plotClusterLegend(cset,dir="v",xlab="CL")
    image_matrix(cnrm, col=gray.colors(100, end=0, start=1),ylab=NA,xlab="time, h")
    axis(1, at=1:ncol(cnrm), labels=times,mgp=c(.5,.3,0),tcl=-.1, las=2,cex.axis=.5)
    mtext("time, h", 1,1.3)
    if ( !missing(file.name) )
        dev.off()
}

## generic cluster plot function 
plotABCD <- function(data, time, file.base, norm, ylab="unknown", ref.xy, ref.ylab="DO, %", sets=c("acd","ab","bcd","abcd"), legend, do.lg2r=TRUE,...) {
    for ( set in sets ) {
        cls.srt <- get(paste0("coarse.srt.",set),mode="character")
        if ( !missing(file.base) ) {
            file.name <- paste0(file.base,set,"_all")
            plotdev(file.name, type=fig.type,res=300,width=6,height=2.25)
        }
        par(mai=c(0.45,.5,.1,.5), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i")
        segmenTools::plotClusters(data,coarse.cls,cls.srt=cls.srt,cls.col=coarse.col,each=F,q=q,type="all",avg.col=NA,avg.pch=NA,time=time,xlab="time, h",ylab=ylab,ref.xy=ref.xy,ref.ylab=ref.ylab,lwd=1,...)
        if ( !missing(legend) )
            legend("bottom",legend=legend,box.col=NA, bg="#FFFFFFCC")
        if ( !missing(file.base) ) dev.off()
        for ( q in c(0.0, 0.8) ) {
            if ( !missing(file.base) ) {
                file.name <- paste0(file.base,set,"_q", sub("\\.","",round(q,2)))
                plotdev(file.name, type=fig.type,res=300,width=6,height=2.25)
            }
            par(mai=c(0.45,.5,.1,.5), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i")
            segmenTools::plotClusters(data,coarse.cls,cls.srt=cls.srt,cls.col=coarse.col,each=F,q=q,type="rng",avg.pch=NA,time=time,xlab="time, h",ylab=paste(ylab, "- cluster mean"),ref.xy=ref.xy,ref.ylab=ref.ylab,...)
            if ( !missing(legend) )
                legend("bottom",legend=legend,box.col=NA, bg="#FFFFFFCC")
           if ( !missing(file.base) )  dev.off()
            if ( do.lg2r ) {
                if ( !missing(file.base) ) {
                    file.name <-
                        paste0(file.base,set,"_q", sub("\\.","",round(q,2)),"_lg2r")
                    plotdev(file.name, type=fig.type,res=300,width=6,height=2.25)
                }
                par(mai=c(0.45,.5,.1,.5), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i")
                segmenTools::plotClusters(data,coarse.cls,cls.srt=cls.srt,cls.col=coarse.col,each=F,q=q,type="rng",avg.pch=NA,time=time,xlab="time, h",ylab=paste("log2 ratio - cluster mean"),norm="lg2r",ref.xy=ref.xy,ref.ylab=ref.ylab,...)
                if ( !missing(legend) )
                    legend("bottom",legend=legend,box.col=NA, bg="#FFFFFFCC")
                if ( !missing(file.base) ) dev.off()
            }
        }
    }
}



### TU/LI ORIGINAL DATA

cat(paste("\nPLOTTING CLUSTERED TIME SERIES\n\n"))

##fig.path <- file.path(out.path,"figures","tuli" )
##dir.create(fig.path,recursive=TRUE)

setnam <- list(li06="IFO 0233, D=0.086 h-1",
               tu05="CEN.PK 122, D=0.09-0.1 h-1")
setcyc <- c(li06=4,tu05=3)
## CLUSTER AVERAGE TIMESERIES

for ( dataID in names(setnam) ) {
    cat(paste("\tDATA:", dataID, dataTyp, "total\n"))
    ## RNA timecourses
    genes.ts <- get(paste(dataID, "genes", dataTyp, sep="."))
    
    ## LOG2 MEAN RATIO
    genes.nrm <-  log2(genes.ts/rowMeans(genes.ts,na.rm=TRUE))
    ## MEAN-0 NORMALIZATION
    genes.nrm <- t(apply(genes.ts,1,scale))

    ## average expression
    ## li06: take only first cycle - 12 timepoints
    ## tu05: take only first cycle - 13 timepoints
    cut <- 1:ncol(genes.ts)
    if ( dataID=="li06" ) cut <- 1:12
    if ( dataID=="tu05" ) cut <- 1:13
    genes.avg <-apply(genes.ts, 1, mean, na.rm=TRUE)
    genes.avgl <- append(genes.avgl, list(genes.avg))
    names(genes.avgl)[length(genes.avgl)] <- dataID
    file.name <- file.path(fig.path,paste0(dataID,"_average"))
    plotdev(file.name, type=fig.type,width=4,height=2.5)
    par(mai=c(.5,.5,.05,.05))
    boxplot(ash(genes.avg) ~ factor(coarse.cls, levels=coarse.srt),log="")
    dev.off()

    
    ## time in minutes
    zerophase <- abs(phase.shift[dataID]) * exact.periods[dataID]/360
    time <- 0:(ncol(genes.ts)-1) * sample.times[dataID] 
    xticks <- seq(0,exact.periods[dataID]*cycle.numbers[dataID],
                  exact.periods[dataID])
                                        # in hours
    tuli.times <- time/60
    
    ## DOT timecourse
    ## OVERRIDE Tu et al. with re-digitized
    if ( dataID=="tu05" ) {
        oxym <- read.csv(file.path(chemo.path,"originalData","tu05_fig2b.csv"))
        oxym[,1] <- oxym[,1]/60
        
    } else {
        ## STORE IN RData - todo re-digitize
        oxy <- get(paste(dataID,".refData",sep=""))
        oxym <- cbind(tuli.times,oxy)
    }
    
    ## sorted gene table
    plotGenes(as.matrix(genes.ts),cset,round(tuli.times,1),
              cycles=setcyc[dataID],
              file.name=file.path(fig.path,paste0(dataID,"_genes")))

    ## time-series
    ##file.base <- file.base=file.path(fig.path,paste0(dataID,"_", dataTyp,"_timecourse_ranges_"))
    file.base <- file.path(fig.path,paste0(dataID,"_ranges_",sep=""))
    plotABCD(data=as.matrix(genes.ts), time=tuli.times,
             file.base=file.base, norm="", ylab="fluor., a.u.", ref.xy=oxym)

    ## DFT Components
    tset <- segmenTier::processTimeseries(genes.ts, use.fft=TRUE)
    cycles <- 1:4
    cols <- coarse.col.major## only major clusters; and with alpha
    cols[!is.na(cols)] <-
        add_alphas(cols[!is.na(cols)],rep(.5,sum(!is.na(cols))))

    file.name <- file.path(fig.path,paste0(dataID,"_polarplot",sep=""))
    plotdev(file.name, type=fig.type,width=12,height=2.5)
    par(mfcol=c(1,4),mai=c(.5,.5,.5,.01),mgp=c(1.1,.2,0),tck=-.03,cex=1.1)
    tmp <- plotDFT(tset,col=cols, lambda=1, cycles=cycles,
                   radius=1,asp=1,pch=19,cex=.5)
    dev.off()

    ## temporary for defense talk:
    ## visualize effect of box-cox
    for ( lambda in c(1,.9,.7,.5,.1) ) {
        file.name <- file.path(fig.path,paste0(dataID,"_polarplot_L",
                                               sub("\\.","",lambda),sep=""))
        plotdev(file.name, type=fig.type,width=12,height=2.5)
        par(mfcol=c(1,4),mai=c(.5,.5,.5,.01),mgp=c(1.1,.2,0),tck=-.03,cex=1.1)
        tmp <- plotDFT(tset,col=cols, lambda=lambda, cycles=cycles,
                       radius=1,asp=1,pch=19,cex=.5)
        dev.off()
    }
    ## visualize effect of amplitude box-cox
    for ( lambda in c(1,.9,.7,.5,.1) ) {
        file.name <- file.path(fig.path,paste0(dataID,"_polarplot_amp_L",sub("\\.","",lambda),sep=""))
        plotdev(file.name, type=fig.type,width=12,height=2.5)
        par(mfcol=c(1,4),mai=c(.5,.5,.5,.01),mgp=c(1.1,.2,0),tck=-.03,cex=1.1)
        tmp <- plotDFT(tset,col=cols, lambda=lambda, cycles=cycles, radius=1,asp=1,pch=19,cex=.5,bc="amplitude")
        dev.off()
    }
}


### SLAVOV et al. 2011: OSCILLATION IN ETHANOL BATCH - TRANSCRIPTOME

## LOAD DATA
sl11 <- read.delim(file.path(yeast.path,"originalData","ymc_eth_raw_centered_ben.cdt"), header=T)
## remove duplicated, TODO: better? 
sl11 <- sl11[!duplicated(sl11[,"YORF"]),]
rownames(sl11) <- sl11[,"YORF"]

## load digitized oxygen trace
sl11ox <- read.csv(file.path(chemo.path,"originalData","slavov11b_fig3B_cut.csv"))
sl11ox[,1] <- sl11ox[,1]/60

## get time series
sl11.ts.raw <- grep("Ethanol.Batch..Raw",colnames(sl11))
sl11.raw <- sl11[,sl11.ts.raw] # raw data

## map to clustering
sl2cls <- match(names(coarse.cls), rownames(sl11))
sl11.raw <- sl11.raw[sl2cls,]
rownames(sl11.raw) <- names(coarse.cls)

## data is from Cy3/Cy5 microarray, where reference cRNA
## is from `a glucose-limited, asynchronous culture growing exponentially at growth rate μ = 0.25 h−1'; and likely the log2 of the ratio:
## de-log for raw data view!
sl11.raw <- 2^sl11.raw

## get time points
sl11.times <- sub(".min","", sub("Ethanol.Batch..Raw..t...","",
                                 colnames(sl11.raw)))
sl11.times <- as.numeric(sl11.times)/60
sl11.times <- sl11.times - min(sl11.times)
colnames(sl11.raw) <- round(sl11.times,1)

## average expression
## unequal sampling times: interpolate to equally spaced times; use minimal
## time-step to capture full resolution!
ntime <- seq(0,max(sl11.times),min(diff(sl11.times)))
genes.itpl <- t(apply(as.matrix(sl11.raw), 1, function(x) {
    if( sum(!is.na(x))>2 ) return(approx(x=sl11.times, y=x, xout=ntime)$y)
    else return(rep(NA,length(ntime)))}))
## cut first cycle : time < 4.65
maxtime <- 4.65
genes.avg <- apply(genes.itpl[,ntime<maxtime], 1, mean, na.rm=TRUE)
genes.avgl <- append(genes.avgl, list(genes.avg))
names(genes.avgl)[length(genes.avgl)] <- "slavov11"
file.name <- file.path(fig.path,paste0("slavov11","_average"))
plotdev(file.name, type=fig.type,width=4,height=2.5)
par(mai=c(.5,.5,.05,.05))
boxplot(ash(genes.avg) ~ factor(coarse.cls, levels=coarse.srt),log="")
dev.off()
 


## sorted gene table
plotGenes(sl11.raw,cset,round(sl11.times,1),cycles=3,file.name=file.path(fig.path,paste("slavov11_genes",sep="")))

## timeseries
plotABCD(data=as.matrix(sl11.raw), time=sl11.times, file.base=file.path(fig.path,paste("slavov11_ranges_",sep="")), norm="", ylab="Cy5/C3", ref.xy=sl11ox)


## KUANG et al. 2014 - LONG PERIOD RNAseq

library(segmenTools)

## LOAD DATA
## NOTE: already normalized, mean-centered
ku14 <- read.csv(file.path(yeast.path,"processedData","kuang14_rnaseq.csv"), header=T)
## remove duplicated, TODO: better? 
ku14 <- ku14[!duplicated(ku14[,"Gene.name"]),]
rownames(ku14) <- ku14[,"Gene.name"]
ku14 <- ku14[,2:17]



## load digitized oxygen trace
ku14ox <- read.csv(file.path(chemo.path,"originalData","kuang14_fig1a.csv"))

## get time series
ku14.times <- unlist(read.csv(file.path(yeast.path,"processedData","kuang14_times.csv")))

## map to clustering
ku2cls <- match(names(coarse.cls), rownames(ku14))


## average expression
genes.avg <- apply(ku14[ku2cls,], 1, mean, na.rm=TRUE)
genes.avgl <- append(genes.avgl, list(genes.avg))
names(genes.avgl)[length(genes.avgl)] <- "kuang14"
file.name <- file.path(fig.path,paste0("kuang14","_average"))
plotdev(file.name, type=fig.type,width=4,height=2.5)
par(mai=c(.5,.5,.05,.05))
boxplot(ash(genes.avg) ~ factor(coarse.cls, levels=coarse.srt),log="")
dev.off()
 

## sorted gene table
plotGenes(as.matrix(ku14[ku2cls,]),cset,round(ku14.times,1),cycles=1,file.name=file.path(fig.path,paste("kuang14_genes",sep="")))


## time-series
plotABCD(data=as.matrix(ku14[ku2cls,]), time=ku14.times, file.base=file.path(fig.path,paste("kuang14_ranges_",sep="")), norm="", ylab="mean-0", ref.xy=ku14ox)

## WANG et al. 2015 - LOW vs. HIGH DILUTION RATE TRANSCRIPTOMES
## NOTE: 0-mean normalization
## RPKM values with HOMER (Heinz et al., 2010);
## normalized by loess regression to non-cycling genes!

library(segmenTools)

## high and low dilution rate datasets
wang.cols <- list(highD=10:29, lowD=10:33)
wang.cuts <- list(highD=1:11, lowD=1:13) # one cycle! 
setcyc <- c(highD=2,lowD=2)
setnam <- list(highD="CEN.PK 122, D=0.12-0.13 h-1",
               lowD="CEN.PK 122, D=0.045-0.05 h-1")
for ( set in c("highD","lowD") ) {
    
    file.name <- file.path(yeast.path,"processedData/",
                           paste("wang15_",set,".csv",sep=""))
    wang15 <- read.csv(file.name,skip=3,header=T)
    rownames(wang15) <- wang15[,1] 
    wang15 <- wang15[,wang.cols[[set]]] # get time series
    
    
    ## time axis
    wang15.times <- as.numeric(sub("T","",colnames(wang15)))
    wang15.times <- wang15.times - min(wang15.times)
    ## map to clusters
    wang2cls <- match(names(coarse.cls), rownames(wang15))
    ## resort
    
    ## oxygen data from ChemostatData
    oxy <- read.csv(file.path(chemo.path, "originalData",
                              paste("wang15_figS4a_",sub("D","",set),
                                    ".csv",sep="")))
    oxy[,1] <- oxy[,1] - oxy[1,1]

    ## average expression
    ## only first cycle
    genes.avg <- apply(as.matrix(wang15[wang2cls,wang.cuts[[set]]]), 1, mean, na.rm=TRUE)
    genes.avgl <- append(genes.avgl, list(genes.avg))
    names(genes.avgl)[length(genes.avgl)] <- paste0("wang15_",set)
    file.name <- file.path(fig.path,paste0("wang15_",set,"_average"))
    plotdev(file.name, type=fig.type,width=4,height=2.5)
    par(mai=c(.5,.5,.05,.05))
    boxplot(ash(genes.avg) ~ factor(coarse.cls, levels=coarse.srt),log="")
    dev.off()
 

    ## sorted gene table
    plotGenes(as.matrix(wang15[wang2cls,]),cset,round(wang15.times,1),
              cycles=setcyc[set],
              file.name=file.path(fig.path,paste0("wang15_",set,"_genes")))

    ## time-series
    plotABCD(data=as.matrix(wang15[wang2cls,]), time=wang15.times,
             file.base=file.path(fig.path,paste0("wang15_",set,"_ranges_",
                                                 sep="")),
             norm="", ylab="RPKM", ref.xy=oxy)
}


## NOCETTI&WHITEHOUSE et al. 2016: OSCILLATION TRANSCRIPTOME
## WITH MNase TIMESERIES

library(segmenTools)


## load data
## NOTE: normalized RPKM values
noc16 <- read.csv(file.path(yeast.path,"processedData/nocetti16_transcriptome.csv"), header=T,row.names=1)
## log2 mean ratio normalization!!
noc16.nrm <- log2(noc16/apply(noc16,1,mean))
## mean-0 normalization
noc16.nrm <- t(apply(noc16,1,scale))
noc16.times <- 1:ncol(noc16) ## TODO: get actual time

## load oxygen data and sampling times (digitized by engauge from fig 1b)
## get time series
noc16ref <- read.csv(file.path(chemo.path,"originalData",
                               "nocetti16_fig1b.csv"))
noc16.times <- noc16ref[which(!is.na(noc16ref[,"samples"])),1]/60
noc16ox <- noc16ref[!is.na(noc16ref[,2]),1:2]
noc16ox[,1] <- noc16ox[,1]/60

## map to clustering
noc162cls <- match(names(coarse.cls), rownames(noc16))

## average expression
## unequal sampling times: interpolate to equally spaced times; use minimal
## time-step to capture full resolution!
ntime <- seq(0,max(noc16.times),min(diff(noc16.times)))
genes.itpl <- t(apply(as.matrix(noc16), 1, function(x)
    approx(x=noc16.times, y=x, xout=ntime)$y))
genes.avg <- apply(genes.itpl[noc162cls,], 1, mean, na.rm=TRUE)
genes.avgl <- append(genes.avgl, list(genes.avg))
names(genes.avgl)[length(genes.avgl)] <- "nocetti16"
file.name <- file.path(fig.path,paste0("nocetti16","_average"))
plotdev(file.name, type=fig.type,width=4,height=2.5)
par(mai=c(.5,.5,.05,.05))
boxplot(ash(genes.avg) ~ factor(coarse.cls, levels=coarse.srt),log="")
dev.off()
 

## sorted gene table
plotGenes(as.matrix(noc16[noc162cls,]),cset,round(noc16.times,1),cycles=1,file.name=file.path(fig.path,paste0("nocetti16_genes")))

## time-series
plotABCD(data=as.matrix(noc16[noc162cls,]), time=noc16.times, file.base=file.path(fig.path,paste0("nocetti16_ranges_",sep="")), norm="", ylab="RPKM", ref.xy=noc16ox)


### DIAUXIE DATA



### CELL CYCLE DATA from BRISTOW et al. 2014 and ORLANDO et al. 2008

## TODO: preprocess data externally!?

## LOAD Orlando et al. 2008 data
#!series_matrix_table_begin
## header line: 69
## expID line: 40 ("!Sample_source_name_ch1")
## strain line: 42 ("!Sample_characteristics_ch1")
## platform ID: GPL2529 - [Yeast_2] Affymetrix Yeast Genome 2.0 Array
## use outdated file processedData/yeast2.annot.csv for mapping
orlando08 <- read.delim(file.path(yeast.path,"originalData","GSE8799_series_matrix.txt"),skip=68, header=TRUE,row.names=1)
annot <- read.delim(file.path(yeast.path,"processedData","yeast2.annot.csv"),header=FALSE)
datNames <- as.character(annot[match(rownames(orlando08), annot[,1]),2])
idx <- which(datNames %in% names(coarse.cls))
orlando08 <- orlando08[idx,]
datNames <- datNames[idx]
## take only first of duplicated
orlando08 <- orlando08[!duplicated(datNames),]
datNames <- datNames[!duplicated(datNames)]
rownames(orlando08) <- datNames
## take only clustered
orlando08 <- orlando08[names(coarse.cls),]
rownames(orlando08) <- names(coarse.cls)


## experiment IDs
# !Sample_title
orlando08exp <- unlist(read.delim(file.path(yeast.path,"originalData","GSE8799_series_matrix.txt"),skip=33, header=FALSE,colClasses="character")[1,])
orlando08exp <- orlando08exp[2:length(orlando08exp)]
## align experiment names with bristow14 names, such that
## filters in data selection work
orlando08exp <- sub("WildType", "WT",sub("CyclinMutant","clb",sub("min"," min", gsub("_", ", ", orlando08exp))))
colnames(orlando08) <- orlando08exp


## LOAD Bristow et al. 2014 data
#!series_matrix_table_begin
## header line: 67
## expID line: 40 ("!Sample_source_name_ch1")
## strain line: 42 ("!Sample_characteristics_ch1")
## platform ID: GPL2529 - [Yeast_2] Affymetrix Yeast Genome 2.0 Array
## use outdated file processedData/yeast2.annot.csv for mapping
bristow14 <- read.delim(file.path(yeast.path,"originalData","GSE49650_series_matrix.txt"),skip=66, header=TRUE,row.names=1)
annot <- read.delim(file.path(yeast.path,"processedData","yeast2.annot.csv"),header=FALSE)
datNames <- as.character(annot[match(rownames(bristow14), annot[,1]),2])
idx <- which(datNames %in% names(coarse.cls))
bristow14 <- bristow14[idx,]
datNames <- datNames[idx]
## take only first of duplicated
bristow14 <- bristow14[!duplicated(datNames),]
datNames <- datNames[!duplicated(datNames)]
rownames(bristow14) <- datNames
## take only clustered
bristow14 <- bristow14[names(coarse.cls),]
rownames(bristow14) <- names(coarse.cls)


## experiment IDs
bristow14exp <- unlist(read.delim(file.path(yeast.path,"originalData","GSE49650_series_matrix.txt"),skip=39, header=FALSE,row.names=1,colClasses="character")[1,])
colnames(bristow14) <- bristow14exp

### combine data from Orlando et al. 2008 and Bristow et al. 2014
bristow14 <- cbind(orlando08,bristow14)

sets <- c("WT","clb","cdc20","cdc8","cdc8cdc20","cse4","rad53")
setnam <- c(expression("wild type: G1-synchronized by elutriation"), 
            expression(italic("clb")~": no S-phase or mitotic cyclins"),
            expression(italic("cdc20")~Delta~": metaphase-to-anaphase arrest"),
            expression(italic("cdc8")^"ts"~": thymidine depletion"),
            expression(italic("cdc8")^"ts"~";"~italic("cdc20")~Delta),
            expression(italic("P")["GAL1"]~italic("-cse4-353")~": spindle disruption"),
            expression(italic("rad53-1")~": no DNA repl. checkpoint signal"))
names(setnam) <- sets
## clb: clb1,2,3,4,5,6 GAL1-CLB1; does not express S-phase and mitotic cyclins.
## Cdc20: cdc20Δ;PGALL-CDC20; arresting cells at the metaphase-to-anaphase transition with persistent levels of Clb2 protein and Clb2/Cdk1 activity 
## Cdc8: cdc8ts; temperature sensitive allele of the thymidylate kinase gene, cdc8, to deplete nucleotides
## Cse4: PGAL1-cse4-353; constitutive over-expression of a mutant allele of the kinetochore protein, CSE4, which disrupts spindle organization; 
## Rad53: cdc20Δ;PGAL1-CDC20;cdc8ts;rad53-1; defective in DNA replication checkpoint signaling
for ( rep in c("rep1","rep2") ) 
    for ( set in sets ) {
        
        ## get experiment - only replicate 1
        ## TODO: average of two replicates?
        ## NOTE: only 1 replicate for cdc8cdc20
        idx <- grep(paste(rep,"$",sep=""),grep(paste(", *",set," *,",sep=""),
                                               colnames(bristow14),value=T),value=T)
        if ( length(idx)==0) next
        br14 <- bristow14[,idx]
        ## get time in hours
        br14.times <- as.numeric(sub(".*, *(.*) min, .*","\\1",idx))/60 
      
        ## log2 mean ratio normalize
        br14.nrm <- log2(br14/rowMeans(br14,na.rm=T))
        ## MEAN-0 NORM
        br14.nrm <- t(apply(br14,1,scale))

        ## TODO: plotABCD!
        file.base <- file.path(fig.path, paste0("bristow14_",set,"_ranges_"))

        plotABCD(data=br14.nrm, time=br14.times, file.base=file.base,
                 norm="", ylab="mean-0 norm.",sets="major",
                 legend=setnam[set]) #, ref.xy=oxy)
        
      
    }




## CHIN et al. 2012 - CEN.PK113 2h vs. 4h oscillation!
## OSCILLATION TRANSCRIPTOME CEN.PK113-7D - 2h vs. 4h

library(segmenTools)

files <- c("chin12_2h.csv","chin12_4h.csv")
o2files <- c("chin12_fig1a.csv", "chin12_fig1c.csv")
names(o2files) <- files

for ( file in files ) {
    id <- sub(".csv","",file)
    ## load data
    ch12 <- read.csv(file.path(yeast.path,"processedData",file),
                     header=T,row.names=1)

    ## TODO: array-to-array norm!
    #sd <- apply(ch12,1,sd)
    #lvs.thresh <- quantile(sd,.01,na.rm=T)
    #los <- sd<lvs.thresh
    #los[is.na(los)] <- FALSE
    #rowMedians <- function(x) apply(x,1,median,na.rm=TRUE) 
    #rowMeans <- function(x) apply(x,1,mean,na.rm=TRUE) 
    #ch12.los <- normalize.los(ch12,los)

    ## NORMALIZE
    ## subtract array median from each array
    ## and add back global median
    ch12.med <- apply(ch12,2,median,na.rm=TRUE)
    ch12 <- t(t(ch12) - ch12.med + median(ch12.med))
    
    ch12.times <- as.numeric(gsub("X","",gsub("\\.h", "", colnames(ch12))))
    
    ## load oxygen data (digitized by engauge from fig 1)
    ## NOTE: same time-frame as ch12.times but longer on both ends!
    ## use range(ch12.times) as xlim!
    oxy <- read.csv(file.path(chemo.path,"originalData",o2files[file]))
    
    
    ## map to clustering
    ch122cls <- match(names(coarse.cls), rownames(ch12))

    ## average expression
    ## chin12_2h : cut last 3 timepoints to get one cycle
    ## chin12_4h : seems ok : experiment is one cycle
    cut <- 1:ncol(ch12)
    if ( id=="chin12_2h" ) cut <- 1:(ncol(ch12)-3)
    genes.avg <- apply(as.matrix(ch12[ch122cls,cut]), 1, mean, na.rm=TRUE)
    genes.avgl <- append(genes.avgl, list(genes.avg))
    names(genes.avgl)[length(genes.avgl)] <- id
    file.name <- file.path(fig.path,paste0(id,"_average"))
    plotdev(file.name, type=fig.type,width=4,height=2.5)
    par(mai=c(.5,.5,.05,.05))
    boxplot(ash(genes.avg) ~ factor(coarse.cls, levels=coarse.srt),log="")
    dev.off()
    
    ## sorted gene table
    plotGenes(as.matrix(ch12[ch122cls,]),cset,round(ch12.times,1),cycles=1,file.name=file.path(fig.path,paste0(id,"_genes")))

    ## time-series
    plotABCD(data=as.matrix(ch12[ch122cls,]), time=ch12.times, file.base=file.path(fig.path, paste0(id,"_ranges_")), norm="", ylab="fluor. a.u.", ref.xy=oxy)

 
}




### SLAVOV et al. 2014
## constant growth in batch with increasing glycolysis
## NOTE 368 clustered genes missing from slavov14 transcript data and 191 sl14 transcripts missing from clusters

##fig.path <- file.path(out.path,"figures","diauxie") 
##dir.create(fig.path,recursive=TRUE)

## load cells/mL to show growth curves
cells <- read.csv(file.path(chemo.data,"slavov14_figS2B.csv"))
## shift t=0 to diauxic shift
shift <- 25.5
lag <- 7
cells[,1] <- cells[,1] - shift

##cells[,2] <- log(cells[,2])
cells[,2] <- cells[,2]/1e6

sets <- c("transcripts","proteins","phosphorylated","acetylated","methylated")
dats <- c("phase1","phase2")

## TODO: compare transcript vs. protein levels at each time-bound
## find a transition to translational regulation??

for ( set in sets ) {

    ## read and process data
    for ( dat in dats ) {
        tmp <- paste("slavov14_",dat,"_",set,".dat",sep="")
        tmp <- file.path(yeast.path,"processedData",tmp)
        tmp <- read.delim(tmp,header=TRUE)
        ## data columns
        datcols <- grep("h$",colnames(tmp)) 
        ## average of duplicated
        ## TODO: just take first of duplicated? 
        dupls <- duplicated(tmp[,1])
        rms <- NULL
        for ( dupl in which(dupls) ) {
            idx <- which(tmp[,1] == tmp[dupl,1])
            tmp[idx[1],datcols] <- colMeans(tmp[idx,datcols])
            rms <- c(rms,idx[-1])
        }
        if ( !is.null(rms) ) tmp <- tmp[-rms,]
        ## assign rownames and rm all know data columns
        rownames(tmp) <- tmp[,1]
        tmp <- tmp[,datcols]
        assign(dat, tmp)
    }
    
    ## ranges
    ph1 <- 1:ncol(phase1)
    ph2 <- 1:ncol(phase2) + ncol(phase1)
    
    ## map to clustering
    sl14 <- cbind(phase1[names(coarse.cls),],phase2[names(coarse.cls),])
    sl14 <- sl14[,colnames(sl14)!="X"]
    rownames(sl14) <- names(coarse.cls)
    ## sample times in h
    sl14.times <- as.numeric(gsub("X","",gsub("\\.h","",colnames(sl14))))
    ## shift by diauxic shift
    sl14.times <- sl14.times - shift
    
    ## TODO: better re-create the original nice plot
    ## from tataProject/yeast/scripts/corecarbon.R
    ## allow splitting between diauxic phases (don't connect poitns)
    file.base <- file.path(fig.path, paste0("slavov14_",set,"_ranges_"))
    plotABCD(data=as.matrix(sl14), time=sl14.times, file.base=file.base, norm="", ylab="todo", do.lg2r=FALSE, ref.xy=cells,ref.ylab=expression(10^6~" cells/mL"),ref.col="#00000055")
}

save(genes.avgl, file=file.path(fig.path,"transcriptomes.RData"))

## SUMMARY RESULTS

out.path <- "~/work/ChemostatData/"
fig.path <- "~/work/ChemostatData/figures/transcriptomes"

load(file.path(fig.path,"transcriptomes.RData"))

## load period data : get dilution rate etc.
## TODO: use this data to cut one cycle!?
periods <- read.csv(file.path(out.path,"periods.csv"),
                    head=T, stringsAsFactors=FALSE, comment.char = "#")
transcriptomes <- c("slavov11b_figS1A_2_main","li06","tu05",
                    "chin12_2h","chin12_4h",
                    "wang15_lowD","wang15_highD","nocetti16")
periods <- periods[periods[,"osc.ID"]%in%transcriptomes,]
periods[,"osc.ID"] <- sub("b_figS1A_2_main","",periods[,"osc.ID"])
## growth rates
gr <- as.numeric(periods[,"D..1.h"])
pr <- as.numeric(periods[,"P..h"])
names(gr) <- names(pr) <- periods[,"osc.ID"]
#gr["kuang14"] <- .095

## SYMBOL BY METHOD: RNAseq vs. microarray
## TODO: align with plot symbols in transcriptomes plot from collectPeriods.R
pch <- rep(20, length(gr))
col <- rep(1, length(gr))
names(pch) <- names(col) <- names(gr)
col[c("wang15_lowD","wang15_highD","nocetti16")] <- 2
pch[c("wang15_lowD","wang15_highD","nocetti16")] <- 18

## AVERAGE CLUSTER A/D RATIOS
## TODO:
## * nocetti16: interpolate to equally spaced time-points
## * check raw data type
## * calculate growth rate by grad_descent method and compare

## NOTE: median brings RNAseq data closer, but less steap increase
## for wang15 - higher D expression in RNAseq/saturated in microarray?
cls.avgl <- lapply(genes.avgl, function(x) sapply(coarse.srt, function(y) {
    mean(x[coarse.cls==y],na.rm=TRUE)}))
ad <- unlist(lapply(cls.avgl, function(x) unname(log2(x["A"]/x["D"]))))
abd <- unlist(lapply(cls.avgl, function(x) unname(log2(x["AB"]/x["D"]))))

ad <- ad[names(gr)]
abd <- abd[names(gr)]

df <- data.frame(mu=gr, "AtoD"=ad) #"AtoD"=ad,  
file.name <- file.path(fig.path,"AtoD_vs_mu")
plotdev(file.name,type=fig.type,width=5,height=3)
par(mai=c(0.5,.6,0.1,0.1),mgp=c(1.3,.3,0),tcl=-.3)
plot(df, col=1:nrow(df),pch=pch[rownames(df)],cex=1.5,
     ylab="log2(A/D)", xlab=expression("dilution rate "~phi~", h"^-1))
#points(df)
legend("topleft",rownames(df), col=1:nrow(df), pch=pch[rownames(df)], pt.cex=1.5,
       box.lwd=0, bg=NA)
legend("bottomright", c("microarray","RNAseq"), pch=c(20,18), pt.cex=1.5,
       box.lwd=0, bg=NA)
lines(df[grep("chin12",rownames(df)),])
lines(df[grep("wang",rownames(df)),])
dev.off()

df <- data.frame(period=pr, "AtoD"=ad) #"AtoD"=ad,  
file.name <- file.path(fig.path,"AtoD_vs_tosc")
plotdev(file.name,type="pdf",width=5,height=3)
par(mai=c(0.5,.6,0.1,0.1),mgp=c(1.3,.3,0),tcl=-.3)
plot(df, col=1:nrow(df),pch=pch[rownames(df)],cex=1.5,
     ylab="log2(A/D)", xlab="period, h")
#points(df)
legend("topleft",rownames(df), col=1:nrow(df), pch=pch[rownames(df)], pt.cex=1.5,
       box.lwd=0, bg=NA)
legend("bottomright", c("microarray","RNAseq"), pch=c(20,18), pt.cex=1.5,
       box.lwd=0, bg=NA)
lines(df[grep("chin12",rownames(df)),])
lines(df[grep("wang",rownames(df)),])
dev.off()

df <- data.frame(mu=gr, "ABtoD"=abd) 

file.name <- file.path(fig.path,"ABtoD_vs_mu")
plotdev(file.name,type="pdf",width=5,height=3)
par(mai=c(0.5,.6,0.1,0.1),mgp=c(1.3,.3,0),tcl=-.3)
plot(df, col=1:nrow(df), pch=pch[rownames(df)], cex=1.5,
     ylab="log2(AB/D)", xlab=expression("dilution rate "~phi~", h"^-1))
#points(df)
legend("topleft",rownames(df),col=1:nrow(df),pch=pch[rownames(df)],
       pt.cex=1.5,
       box.lwd=0, bg=NA)
legend("bottomright", c("microarray","RNAseq"), pch=c(20,18), pt.cex=1.5,
       box.lwd=0, bg=NA)
lines(df[grep("chin12",rownames(df)),])
lines(df[grep("wang",rownames(df)),])
dev.off()


## TCA vor Paula Martinell
tca <- c("NDE1", "NDI1", "CIT1", "IDH1", "IDH2",
         "KGD1", "KGD2", "LSC1", "LSC2", "SDH1", "SDH2", "MDH1")
idx <- match(tca, genes[,"name"])
tca.id <- as.character(genes[idx,"ID"])
coarse.cls[tca.id]
cbind(tca, coarse.cls[tca.id])
