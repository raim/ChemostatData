---
title: On the Period ~ Growth Rate Relation in Budding Yeast Continuous Culture Oscillation
author: Rainer Machn&eacute;
date: 2021-08-01
bibliography: /home/raim/ref/tata.bib
geometry: margin=4cm
---

<!-- pandoc periods.md --filter pandoc-citeproc  -o periods.pdf-->

Soon after Monod, Novick and Szillard had introduced continuous
culture to study microbial growth under constant conditions, Finn and
Wilson observed autosynchronization of metabolic activity and cell
division in a lager yeast (@Finn1954). vMeyenburg, Küenzi and Fiechter
characterized this phenomenon meticulously (@Kuenzi1969,
@vonMeyenburg1969). The period was shorter than the culture doubling
time, a key observation that lead to the clarification of budding
yeast's eponymous asymmetric cell division cycle (CDC) @Meyenburg1969phd
?Mitchison?.  Their decades-long studies concluded with a model where
the budding phase (S, G2, M) is associated with increased respiratory
activity and a short pulse of ethanol secretion
(respiro-fermentation), while the G1 growth phase is characterized by
slower and purely respiratory metabolism, and glycogen synthesis
(@Sonnleitner1986, @Straessle1988, @Muench1992).  The ethanol pulse
was thought to synchronize all cells in the reactor (@Porro1988).


* budding phase constancy vs. period ~ growth rate: sets limits,
constant period, but less amplitude, since a smaller fraction of
cells participate (budding/budding index); shorter periods also observed:
clustering of population into 180°/120° phase-shifted cohorts - mode
model

* population structure models, extension of CDC models @Bellgardt1994a,
@Bellgardt1994b, @Hjortso1995, shorter periods: @Duboc2000, clustering:
CITE, "nice model but data scattered" ... also longer periods, daughter
cell synch?

* however: shorter and longer periods (@Meyenburg1969phd,
@Heinzle1983), synchronization via aeration system; 

* nevertheless: period growth-rate relation @Meyenburg1969phd (Fig. X): provides
mechanism for growth law relation @Slavov2011, @Burnetti2016,

* @Satroutdinov1992, @Keulers1996b, @Keulers1998: short-period, too
fast for CDC-based model, respiratory phase vs respiro-fermentative
phase with ethanol secretion, relations in part reversed, gaseous
synchronizers: co2, h2s, acetaldehyde , oscillations also on
non-fermentable carbon-sources with again altered phase-relations (pH,
ATP),

* but both: glycogen mob/ethanol pulse and S-phase, not energetic!,
it appears as if ifo short period is missing the long RQ=1 phase, 

* @Lloyd2007 et al. interpreted it as a clock, looking for
synchronizers, considered short-period as something distinct from (and
more fundamental than) CDC-coupled,

* @klevecz04: a version of Pittendrigh's "flight from light" hypothesis
on the original function of the circadian clock; opposite was long known,
in fact falsified a year later, where ironically the authors misunderstood
their system and naively copied the Pittendrigh interpretation,

* @Aon2008 period doubling, chaotic, power-law relation of observed periods,
coherent relation of enzyme oscillators up to circadian time-scale
reflects functional coupling,

* short-period can be fitted, eg. by probabilistic CDC model, where
during each cycle 0.06 \% of cells initiate S-phase and budding,

* however: oscillations w/o growth, no synchronization required
in chemostat, also observed in single cells,

* @Burnetti2016: relative HOC/LOC ~ ribosomes, period ~ fermentation
point, LOC phase goes to 0, HOC constant,

* alternative, equally empirical conjecture @Machne2017phd:
  $f_\text{osc}=f_\text{min} e^{\mu \tau}$,

* $f_\text{min}$ at $\mu=0$, and $f_\text{max}$ at $\phi_\text{ferm}$:
sets a boundary for the type of oscillation analyzed herein: beyond,
glycolytic oscillations, membrane potential oscillations, chaotic,

* 4 data sets: rough period doubling at $f_\text{min}$, with an offset,

* what is $\tau$, DBM noted ~ protein half lives,

* protein homeostasis model, where a protein translation pulse occurs
at the HOC/LOC transition, and both, assembly of novel complexes,
protein aggregation and degradation occurs in LOC @ONeill2020,
@Machne2021pre,

* Of course this is again the fashion of the time, LLPS, ATP as hydrotrope,
etc. Cells are complex, likely all aspects true, incl. ROS and oxidative
stress.

## References

